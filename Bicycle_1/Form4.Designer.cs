﻿namespace Bicycle
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("创建客户");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("客户信息管理");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("客户", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("创建联系人");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("联系人信息管理");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("联系人", new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("创建销售订单");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("管理销售订单");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("查看销售订单");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("销售订单", new System.Windows.Forms.TreeNode[] {
            treeNode7,
            treeNode8,
            treeNode9});
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("销售", new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode6,
            treeNode10});
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("创建供应商");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("供应商信息管理");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("供应商", new System.Windows.Forms.TreeNode[] {
            treeNode12,
            treeNode13});
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("创建物料");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("管理物料");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("物料", new System.Windows.Forms.TreeNode[] {
            treeNode15,
            treeNode16});
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("创建采购订单");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("管理采购订单");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("查看采购订单");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("采购订单", new System.Windows.Forms.TreeNode[] {
            treeNode18,
            treeNode19,
            treeNode20});
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("订单入库");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("无单入库");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("入库", new System.Windows.Forms.TreeNode[] {
            treeNode22,
            treeNode23});
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("订单拣料");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("无单拣料");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("拣料", new System.Windows.Forms.TreeNode[] {
            treeNode25,
            treeNode26});
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("收货");
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("发货");
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("物料", new System.Windows.Forms.TreeNode[] {
            treeNode14,
            treeNode17,
            treeNode21,
            treeNode24,
            treeNode27,
            treeNode28,
            treeNode29});
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("创建车间");
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("管理车间信息");
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("车间", new System.Windows.Forms.TreeNode[] {
            treeNode31,
            treeNode32});
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("创建生产计划");
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("管理生产计划");
            System.Windows.Forms.TreeNode treeNode36 = new System.Windows.Forms.TreeNode("查看生产计划");
            System.Windows.Forms.TreeNode treeNode37 = new System.Windows.Forms.TreeNode("生产计划", new System.Windows.Forms.TreeNode[] {
            treeNode34,
            treeNode35,
            treeNode36});
            System.Windows.Forms.TreeNode treeNode38 = new System.Windows.Forms.TreeNode("生产过程");
            System.Windows.Forms.TreeNode treeNode39 = new System.Windows.Forms.TreeNode("生产", new System.Windows.Forms.TreeNode[] {
            treeNode33,
            treeNode37,
            treeNode38});
            System.Windows.Forms.TreeNode treeNode40 = new System.Windows.Forms.TreeNode("销售订单入账");
            System.Windows.Forms.TreeNode treeNode41 = new System.Windows.Forms.TreeNode("资金入账");
            System.Windows.Forms.TreeNode treeNode42 = new System.Windows.Forms.TreeNode("入账", new System.Windows.Forms.TreeNode[] {
            treeNode40,
            treeNode41});
            System.Windows.Forms.TreeNode treeNode43 = new System.Windows.Forms.TreeNode("采购订单出账");
            System.Windows.Forms.TreeNode treeNode44 = new System.Windows.Forms.TreeNode("资金出账");
            System.Windows.Forms.TreeNode treeNode45 = new System.Windows.Forms.TreeNode("出账", new System.Windows.Forms.TreeNode[] {
            treeNode43,
            treeNode44});
            System.Windows.Forms.TreeNode treeNode46 = new System.Windows.Forms.TreeNode("查看账户");
            System.Windows.Forms.TreeNode treeNode47 = new System.Windows.Forms.TreeNode("查看财务记录");
            System.Windows.Forms.TreeNode treeNode48 = new System.Windows.Forms.TreeNode("财务", new System.Windows.Forms.TreeNode[] {
            treeNode42,
            treeNode45,
            treeNode46,
            treeNode47});
            System.Windows.Forms.TreeNode treeNode49 = new System.Windows.Forms.TreeNode("员工信息管理");
            System.Windows.Forms.TreeNode treeNode50 = new System.Windows.Forms.TreeNode("创建公司账户");
            System.Windows.Forms.TreeNode treeNode51 = new System.Windows.Forms.TreeNode("管理公司账户");
            System.Windows.Forms.TreeNode treeNode52 = new System.Windows.Forms.TreeNode("公司账户", new System.Windows.Forms.TreeNode[] {
            treeNode50,
            treeNode51});
            System.Windows.Forms.TreeNode treeNode53 = new System.Windows.Forms.TreeNode("创建物料组");
            System.Windows.Forms.TreeNode treeNode54 = new System.Windows.Forms.TreeNode("管理物料组");
            System.Windows.Forms.TreeNode treeNode55 = new System.Windows.Forms.TreeNode("物料组", new System.Windows.Forms.TreeNode[] {
            treeNode53,
            treeNode54});
            System.Windows.Forms.TreeNode treeNode56 = new System.Windows.Forms.TreeNode("创建分销渠道");
            System.Windows.Forms.TreeNode treeNode57 = new System.Windows.Forms.TreeNode("管理分销渠道");
            System.Windows.Forms.TreeNode treeNode58 = new System.Windows.Forms.TreeNode("分销渠道", new System.Windows.Forms.TreeNode[] {
            treeNode56,
            treeNode57});
            System.Windows.Forms.TreeNode treeNode59 = new System.Windows.Forms.TreeNode("创建交货优先级");
            System.Windows.Forms.TreeNode treeNode60 = new System.Windows.Forms.TreeNode("管理交货优先级");
            System.Windows.Forms.TreeNode treeNode61 = new System.Windows.Forms.TreeNode("交货优先级", new System.Windows.Forms.TreeNode[] {
            treeNode59,
            treeNode60});
            System.Windows.Forms.TreeNode treeNode62 = new System.Windows.Forms.TreeNode("创建发运条件");
            System.Windows.Forms.TreeNode treeNode63 = new System.Windows.Forms.TreeNode("管理发运条件");
            System.Windows.Forms.TreeNode treeNode64 = new System.Windows.Forms.TreeNode("发运条件", new System.Windows.Forms.TreeNode[] {
            treeNode62,
            treeNode63});
            System.Windows.Forms.TreeNode treeNode65 = new System.Windows.Forms.TreeNode("管理", new System.Windows.Forms.TreeNode[] {
            treeNode49,
            treeNode52,
            treeNode55,
            treeNode58,
            treeNode61,
            treeNode64});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.注销ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.BackColor = System.Drawing.Color.White;
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.treeView1.Location = new System.Drawing.Point(23, 84);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Node1_1_1";
            treeNode1.Text = "创建客户";
            treeNode2.Name = "Node1_1_2";
            treeNode2.Text = "客户信息管理";
            treeNode3.Name = "Node1_1";
            treeNode3.Text = "客户";
            treeNode4.Name = "Node1_2_1";
            treeNode4.Text = "创建联系人";
            treeNode5.Name = "Node1_2_2";
            treeNode5.Text = "联系人信息管理";
            treeNode6.Name = "Node1_2";
            treeNode6.Text = "联系人";
            treeNode7.Name = "Node1_3_1";
            treeNode7.Text = "创建销售订单";
            treeNode8.Name = "Node1_3_2";
            treeNode8.Text = "管理销售订单";
            treeNode9.Name = "Node1_3_3";
            treeNode9.Text = "查看销售订单";
            treeNode10.Name = "Node1_3";
            treeNode10.Text = "销售订单";
            treeNode11.BackColor = System.Drawing.Color.White;
            treeNode11.Name = "Node1";
            treeNode11.Text = "销售";
            treeNode12.Name = "Node2_1_1";
            treeNode12.Text = "创建供应商";
            treeNode13.Name = "Node2_1_2";
            treeNode13.Text = "供应商信息管理";
            treeNode14.Name = "Node2_1";
            treeNode14.Text = "供应商";
            treeNode15.Name = "Node2_2_1";
            treeNode15.Text = "创建物料";
            treeNode16.Name = "Node2_2_2";
            treeNode16.Text = "管理物料";
            treeNode17.Name = "Node2_2";
            treeNode17.Text = "物料";
            treeNode18.Name = "Node2_3_1";
            treeNode18.Text = "创建采购订单";
            treeNode19.Name = "Node2_3_2";
            treeNode19.Text = "管理采购订单";
            treeNode20.Name = "Node2_3_3";
            treeNode20.Text = "查看采购订单";
            treeNode21.Name = "Node2_3";
            treeNode21.Text = "采购订单";
            treeNode22.Name = "Node2_4_1";
            treeNode22.Text = "订单入库";
            treeNode23.Name = "Node2_4_2";
            treeNode23.Text = "无单入库";
            treeNode24.Name = "Node2_4";
            treeNode24.Text = "入库";
            treeNode25.Name = "Node2_5_1";
            treeNode25.Text = "订单拣料";
            treeNode26.Name = "Node2_5_2";
            treeNode26.Text = "无单拣料";
            treeNode27.Name = "Node2_5";
            treeNode27.Text = "拣料";
            treeNode28.Name = "Node2_6";
            treeNode28.Text = "收货";
            treeNode29.Name = "Node2_7";
            treeNode29.Text = "发货";
            treeNode30.Name = "Node2";
            treeNode30.Text = "物料";
            treeNode31.Name = "Node3_1_1";
            treeNode31.Text = "创建车间";
            treeNode32.Name = "Node3_1_2";
            treeNode32.Text = "管理车间信息";
            treeNode33.Name = "Node3_1";
            treeNode33.Text = "车间";
            treeNode34.Name = "Node3_2_1";
            treeNode34.Text = "创建生产计划";
            treeNode35.Name = "Node3_2_2";
            treeNode35.Text = "管理生产计划";
            treeNode36.Name = "Node3_2_3";
            treeNode36.Text = "查看生产计划";
            treeNode37.Name = "Node3_2";
            treeNode37.Text = "生产计划";
            treeNode38.Name = "Node3_3";
            treeNode38.Text = "生产过程";
            treeNode39.Name = "Node3";
            treeNode39.Text = "生产";
            treeNode40.Name = "Node4_1_1";
            treeNode40.Text = "销售订单入账";
            treeNode41.Name = "Node4_1_2";
            treeNode41.Text = "资金入账";
            treeNode42.Name = "Node4_1";
            treeNode42.Text = "入账";
            treeNode43.Name = "Node4_2_1";
            treeNode43.Text = "采购订单出账";
            treeNode44.Name = "Node4_2_2";
            treeNode44.Text = "资金出账";
            treeNode45.Name = "Node4_2";
            treeNode45.Text = "出账";
            treeNode46.Name = "Node4_3";
            treeNode46.Text = "查看账户";
            treeNode47.Name = "Node4_4";
            treeNode47.Text = "查看财务记录";
            treeNode48.Name = "Node4";
            treeNode48.Text = "财务";
            treeNode49.Name = "Node5_1";
            treeNode49.Text = "员工信息管理";
            treeNode50.Name = "Node5_2_1";
            treeNode50.Text = "创建公司账户";
            treeNode51.Name = "Node5_2_2";
            treeNode51.Text = "管理公司账户";
            treeNode52.Name = "Node5_2";
            treeNode52.Text = "公司账户";
            treeNode53.Name = "Node5_3_1";
            treeNode53.Text = "创建物料组";
            treeNode54.Name = "Node5_3_2";
            treeNode54.Text = "管理物料组";
            treeNode55.Name = "Node5_3";
            treeNode55.Text = "物料组";
            treeNode56.Name = "Node5_4_1";
            treeNode56.Text = "创建分销渠道";
            treeNode57.Name = "Node5_4_2";
            treeNode57.Text = "管理分销渠道";
            treeNode58.Name = "Node5_4";
            treeNode58.Text = "分销渠道";
            treeNode59.Name = "Node5_5_1";
            treeNode59.Text = "创建交货优先级";
            treeNode60.Name = "Node5_5_2";
            treeNode60.Text = "管理交货优先级";
            treeNode61.Name = "Node5_5";
            treeNode61.Text = "交货优先级";
            treeNode62.Name = "Node5_6_1";
            treeNode62.Text = "创建发运条件";
            treeNode63.Name = "Node5_6_2";
            treeNode63.Text = "管理发运条件";
            treeNode64.Name = "Node5_6";
            treeNode64.Text = "发运条件";
            treeNode65.Name = "Node5";
            treeNode65.Text = "管理";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode11,
            treeNode30,
            treeNode39,
            treeNode48,
            treeNode65});
            this.treeView1.Size = new System.Drawing.Size(243, 493);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // 注销ToolStripMenuItem
            // 
            this.注销ToolStripMenuItem.Name = "注销ToolStripMenuItem";
            this.注销ToolStripMenuItem.Size = new System.Drawing.Size(54, 25);
            this.注销ToolStripMenuItem.Text = "退出";
            this.注销ToolStripMenuItem.Click += new System.EventHandler(this.注销ToolStripMenuItem_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(54, 25);
            this.帮助ToolStripMenuItem.Text = "注销";
            this.帮助ToolStripMenuItem.Click += new System.EventHandler(this.帮助ToolStripMenuItem_Click);
            // 
            // 帮助ToolStripMenuItem1
            // 
            this.帮助ToolStripMenuItem1.Name = "帮助ToolStripMenuItem1";
            this.帮助ToolStripMenuItem1.Size = new System.Drawing.Size(54, 25);
            this.帮助ToolStripMenuItem1.Text = "帮助";
            this.帮助ToolStripMenuItem1.Click += new System.EventHandler(this.帮助ToolStripMenuItem1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.注销ToolStripMenuItem,
            this.帮助ToolStripMenuItem,
            this.帮助ToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 29);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(984, 596);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Form4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ToolStripMenuItem 注销ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem1;
        private System.Windows.Forms.MenuStrip menuStrip1;

    }
}