﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle_1
{
    public partial class CreateCustomerForm : Form
    {
        Dictionary<int, String> DicMaterialGroup = new Dictionary<int, String>();
        Dictionary<int, String> DicDistributionChannel = new Dictionary<int, String>();
        Dictionary<int, String> DicDeliveryPriority = new Dictionary<int, String>();
        Dictionary<int, String> DicShipCondition = new Dictionary<int, String>();
        MMServiceClient MMClient = new MMServiceClient();
        SDServiceClient SDClient = new SDServiceClient();
        public CreateCustomerForm()
        {
            InitializeComponent();

            
            MMClient.getAllMaterialGroupCompleted += new EventHandler<getAllMaterialGroupCompletedEventArgs>(getAllMaterialGroupCallback);
            MMClient.getAllMaterialGroupAsync();

            SDClient.getAllDistributionChannelCompleted += new EventHandler<getAllDistributionChannelCompletedEventArgs>(getAllDistributionChannelCallback);
            SDClient.getAllDistributionChannelAsync();

            SDClient.getAllDeliveryPriorityCompleted += new EventHandler<getAllDeliveryPriorityCompletedEventArgs>(getAllDeliveryPriorityCallback);
            SDClient.getAllDeliveryPriorityAsync();

            SDClient.getAllShipConditionCompleted += new EventHandler<getAllShipConditionCompletedEventArgs>(getAllShipConditionCallback);
            SDClient.getAllShipConditionAsync();

            //DicDistributionChannel = SDClient.getAllDistributionChannel();
            // DicDeliveryPriority = SDClient.getAllDeliveryPriority();
            //DicShipCondition = SDClient.getAllShipCondition();

            


            
            List<String> DistributionChannelValue = new List<String>();
            List<String> DeliveryPriorityValue = new List<String>();
            List<String> ShipConditionValue = new List<String>();
           
            foreach (KeyValuePair<int, String> a in DicDistributionChannel)
            {
                DistributionChannelValue.Add(a.Value);
            }
            comboBox2.DataSource = DistributionChannelValue;
            foreach (KeyValuePair<int, String> a in DicDeliveryPriority)
            {
                DeliveryPriorityValue.Add(a.Value);
            }
            comboBox3.DataSource = DeliveryPriorityValue;
            foreach (KeyValuePair<int, String> a in DicShipCondition)
            {
                ShipConditionValue.Add(a.Value);
            }
            comboBox4.DataSource = ShipConditionValue;


        }

        private void getAllMaterialGroupCallback(object sender, getAllMaterialGroupCompletedEventArgs e)
        {
            List<String> MaterialGroupValue = new List<String>();
            DicMaterialGroup = e.Result;
            foreach (KeyValuePair<int, String> a in DicMaterialGroup)
            {
                MaterialGroupValue.Add(a.Value);
            }
            comboBox1.DataSource = MaterialGroupValue;
            MMClient.Close();
        }


        private void getAllDistributionChannelCallback(object sender, getAllDistributionChannelCompletedEventArgs e)
        {
            List<String> DistributionChannelValue = new List<String>();
            DicDistributionChannel = e.Result;
            foreach (KeyValuePair<int, String> a in DicDistributionChannel)
            {
                DistributionChannelValue.Add(a.Value);
            }
            comboBox2.DataSource = DistributionChannelValue;
        }
        private void getAllDeliveryPriorityCallback(object sender, getAllDeliveryPriorityCompletedEventArgs e)
        {
            List<String> DeliveryPriorityValue = new List<String>();
            DicDeliveryPriority = e.Result;
            foreach (KeyValuePair<int, String> a in DicDeliveryPriority)
            {
                DeliveryPriorityValue.Add(a.Value);
            }
            comboBox3.DataSource = DeliveryPriorityValue;
        }
        private void getAllShipConditionCallback(object sender, getAllShipConditionCompletedEventArgs e)
        {
            List<String> ShipConditionValue = new List<String>();
            DicShipCondition = e.Result;
            foreach (KeyValuePair<int, String> a in DicShipCondition)
            {
                ShipConditionValue.Add(a.Value);
            }
            comboBox4.DataSource = ShipConditionValue;
        }





        private void label1_Click(object sender, EventArgs e)
        {

        }


        private void button1_Click(object sender, EventArgs e)
        {
            String bank = textBox1.Text;
            String company = textBox2.Text;
            String country = textBox3.Text;
            String city = textBox4.Text;
            String address = textBox5.Text;
            String code = textBox6.Text;
            int aa = 0, bb = 0, cc = 0, dd = 0;
            foreach (KeyValuePair<int, String> a in DicMaterialGroup)
            {
                if (a.Value == comboBox1.SelectedItem.ToString())
                {
                    aa = a.Key;
                }
            }
            foreach (KeyValuePair<int, String> a in DicDistributionChannel)
            {
                if (a.Value == comboBox2.SelectedItem.ToString())
                {
                    bb = a.Key;
                }
            }
            foreach (KeyValuePair<int, String> a in DicDeliveryPriority)
            {
                if (a.Value == comboBox3.SelectedItem.ToString())
                {
                    cc = a.Key;
                }
            }
            foreach (KeyValuePair<int, String> a in DicShipCondition)
            {
                if (a.Value == comboBox4.SelectedItem.ToString())
                {
                    dd = a.Key;
                }
            }


            CustomerServiceClient CustomerClient = new CustomerServiceClient();
            int id = CustomerClient.createCustomer(bank,company,city,country,address,code,aa,bb,cc,dd);
            CustomerClient.Close();
            MessageBox.Show("已创建客户 编号为："+id.ToString());
            this.Close();
            //发送客户
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("!!!");
        }


        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
           // MessageBox.Show("!!!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
          //////  MessageBox.Show("!!!");
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
         //   MessageBox.Show("!!!");
        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }

    }
}
