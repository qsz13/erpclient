﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicycleFI
{
    public partial class AccountView2 : Form
    {
        String aid;
        public AccountView2(String a)
        {
            InitializeComponent();
            aid = a;
            MMServiceClient client = new MMServiceClient();
            ACCOUNT now=new ACCOUNT();
            List<ACCOUNT> alist = new List<ACCOUNT>();
            alist = client.getAccountInformation().ToList();
            foreach (ACCOUNT recent in alist)
            {
                if (recent.A_NUMBER.Equals(aid))
                {
                    now = recent;
                }
                label9.Text = now.A_NUMBER;
                label14.Text = now.A_ACCOUNTCUSTOMER;
                label13.Text = now.A_BANK;
                label12.Text = now.A_ACCOUNTCREATETIME;
                label11.Text = now.A_DESCRIPTION;
                label10.Text = now.A_BALANCE;
                label15.Text = now.A_FROZEN;
            }

        }

        private void AccountView2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            AccountView1 a = new AccountView1();
            a.ShowDialog();
        }
    }
}
