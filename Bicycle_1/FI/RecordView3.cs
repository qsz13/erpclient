﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicycleFI
{
    public partial class RecordView3 : Form
    {
        String io=null;
        String id=null;
        public RecordView3(String a,String b)
        {
            InitializeComponent();
            io = a;
            id = b;
            if (io == "入账")
            {
                FIServiceClient fi = new FIServiceClient();
                CAPITAL_IN ci = new CAPITAL_IN();
                ci = fi.getCapital_In(id);
                label13.Text = "入账";
                label12.Text = ci.CI_TIME.ToString();
                label11.Text = ci.CI_INACCOUNTNUM;
                label10.Text = ci.CI_TOACCOUNTNUM;
                label15.Text = ci.CI_AMOUNT.ToString();
                label16.Text = ci.CI_DESCRIPTION;
                label14.Text = ci.CI_INSTAFFID.ToString();
                label17.Text = ci.CI_ID.ToString();
            }
            else
            {
                FIServiceClient fi = new FIServiceClient();
                CAPITAL_OUT co = new CAPITAL_OUT();
                co = fi.getCapital_Out(id);
                label13.Text = "出账";
                label12.Text = co.CO_TIME.ToString();
                label11.Text = co.CO_FROMACCOUNTNUM;
                label10.Text = co.CO_INTOACCOUNTNUM;
                label15.Text = co.CO_AMOUNT.ToString();
                label16.Text = co.CO_DESCRIPTION;
                label14.Text = co.CO_OUTSTAFFID.ToString();
                label17.Text = co.CO_ID.ToString();

            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void RecordView3_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RecordView1 a = new RecordView1();
            a.ShowDialog();
            this.Close();
        }
    }
}
