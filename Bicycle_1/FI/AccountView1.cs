﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicycleFI
{
    public partial class AccountView1 : Form
    {
        public AccountView1()
        {
            InitializeComponent();
            Dictionary<String, decimal> ds = new Dictionary<String, decimal>();
            MMServiceClient client = new MMServiceClient();

            List<ACCOUNT> alist = new List<ACCOUNT>();
            alist = client.getAccountInformation().ToList();

            for (int i = 0; i < alist.Count; i++)
            {
                ds.Add(alist[i].A_NUMBER.ToString(), Convert.ToDecimal(alist[i].A_BALANCE));
            }
            List<String> accountList = new List<String>();
            foreach (KeyValuePair<String, decimal> cc in ds)
            {
                accountList.Add(cc.Key);
            }
            comboBox1.DataSource = accountList;

        }

        private void AccountView1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String chs = comboBox1.SelectedItem.ToString();
            AccountView2 a2=new AccountView2(chs);
            a2.ShowDialog();
            this.Hide();
        }
    }
}
