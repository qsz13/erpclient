﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicycleFI
{
    public partial class RecordView1 : Form
    {
        public RecordView1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime start=dateTimePicker1.Value;
            DateTime end = dateTimePicker2.Value;
            RecordView2 a = new RecordView2(start, end);
            a.ShowDialog();
            this.Hide();
        }
    }
}
