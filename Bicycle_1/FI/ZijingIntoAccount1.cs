﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicycleFI
{
    public partial class ZijingIntoAccount1 : Form
    {
        public ZijingIntoAccount1()
        {
            InitializeComponent();
            Dictionary<String, decimal> ds = new Dictionary<String, decimal>();
            MMServiceClient client = new MMServiceClient();

            List<ACCOUNT> alist = new List<ACCOUNT>();
            alist = client.getAccountInformation().ToList();

            for (int i = 0; i < alist.Count; i++)
            {
                ds.Add(alist[i].A_NUMBER.ToString(), Convert.ToDecimal(alist[i].A_BALANCE));
            }
            List<String> accountList = new List<String>();
            foreach (KeyValuePair<String, decimal> cc in ds)
            {
                accountList.Add(cc.Key);
            }
            comboBox1.DataSource = accountList;

        }

        private void ZijingIntoAccount1_Load(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {


            String inaccount = comboBox1.SelectedItem.ToString();
            String outaccount = textBox1.Text;
            decimal innum = Convert.ToDecimal(textBox3.Text);


            String discrib = textBox2.Text;
            DateTime time = DateTime.Now;
            MMServiceClient client = new MMServiceClient();
            client.createCapitalIn(time, inaccount, outaccount, textBox3.Text, discrib, "278", null);

            FIServiceClient ficlient = new FIServiceClient();
            ACCOUNT a = ficlient.getAccountInformation(inaccount);
            int accountoldmoney = Convert.ToInt32(a.A_BALANCE);
            decimal accountNewMoney = accountoldmoney + Convert.ToDecimal(textBox3.Text);
            ficlient.updateAcountBalance(inaccount, accountNewMoney.ToString());

            //更新订单
            this.Close();
            MessageBox.Show("当前余额为：" + accountoldmoney + "\n打入余额为：" + innum.ToString() + "\n现在余额为：" + accountNewMoney);
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
