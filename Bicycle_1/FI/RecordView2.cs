﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicycleFI
{
    public partial class RecordView2 : Form
    {
        DateTime start;
        int lin = 0;
        DateTime end;
        public RecordView2(DateTime a,DateTime b)
        {
            InitializeComponent();
            start = a;
            end = b;
            FIServiceClient fi = new FIServiceClient();
            List<CAPITAL_IN> ilist=new List<CAPITAL_IN>();
            ilist = fi.getCapInByTime(start, end).ToList();
            List<CAPITAL_OUT> olist=new List<CAPITAL_OUT>();
            olist = fi.getCapOutByTime(start, end).ToList();
            int j=0;
            foreach (CAPITAL_IN ci in ilist)
            {
                dataGridView1.RowCount++;
                dataGridView1.Rows[j].Cells[0].Value = "入账";
                dataGridView1.Rows[j].Cells[1].Value = ci.CI_ID.ToString();
                dataGridView1.Rows[j].Cells[2].Value = ci.CI_TIME.ToString();
                dataGridView1.Rows[j].Cells[3].Value = ci.CI_INACCOUNTNUM;
                dataGridView1.Rows[j].Cells[4].Value = ci.CI_TOACCOUNTNUM;
                dataGridView1.Rows[j].Cells[5].Value = ci.CI_AMOUNT;
                j++;
            }
            foreach (CAPITAL_OUT co in olist)
            {
                dataGridView1.RowCount++;
                dataGridView1.Rows[j].Cells[0].Value = "出账";
                dataGridView1.Rows[j].Cells[1].Value = co.CO_ID.ToString();
                dataGridView1.Rows[j].Cells[2].Value = co.CO_TIME.ToString();
                dataGridView1.Rows[j].Cells[3].Value = co.CO_FROMACCOUNTNUM;
                dataGridView1.Rows[j].Cells[4].Value = co.CO_INTOACCOUNTNUM;
                dataGridView1.Rows[j].Cells[5].Value = co.CO_AMOUNT;
                j++;
            }
            lin = j;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void RecordView2_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int result = 0;
            for (int j = 0; j < lin; j++)
            {
                if (dataGridView1.Rows[j].Cells[6].Value != null)
                {
                    result = j;
                }
            }
            RecordView3 r3 = new RecordView3(dataGridView1.Rows[result].Cells[0].Value.ToString(), dataGridView1.Rows[result].Cells[1].Value.ToString());
            r3.ShowDialog();
            this.Hide();
        }
    }
}
