﻿using Bicycle;
using Bicycle_1;
using BicycleFI;
using BicycleMM;
using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bicycle_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void login_Click(object sender, EventArgs e)
        {
            String nowid = textBox1.Text;
            String nowpassword = textBox2.Text;
            

            //在数据库员工表中找到员工号为nowid，查看密码是否为nowpassword
            StaffServiceClient client = new StaffServiceClient();
            bool result = client.ifexist(nowid);
            
            if (result)
            {
                STAFF_INFORMATION s = client.info(nowid);
                if (s.S_PASSWORD.Equals(nowpassword))
                {
                    this.Hide();
                    User.name = s.S_NAME;
                    Form4 fm2 = new Form4();
                    fm2.Show();
                    User.id = s.S_ID.ToString();
                    User.name = s.S_NAME;
                    User.address = s.S_ADDRESS;
                    User.birthday = s.S_BIRTHDAY.ToString();
                    User.date = s.S_ICDATE.ToString();
                    User.password = s.S_PASSWORD;
                    User.renger = s.S_GENDER;
                    User.telephone = s.S_TEL;
                }
                else
                {
                    MessageBox.Show("密码错误，请重新输入用户id和密码\n");
 
                }
            }
            else
            {
                MessageBox.Show("用户id不存在，请重新输入用户id和密码\n");
            }

            client.Close();
        }

        private void register_Click(object sender, EventArgs e)
        {
            RegisterForm rf = new RegisterForm();
            rf.Show();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //OrdersCapitalOut1 a = new OrdersCapitalOut1(null,1);
            //CreateCustomerForm a = new CreateCustomerForm();
 //           VendorCreate1 a = new VendorCreate1();
            //SalesOrder1 a = new SalesOrder1("");
            ZijingOutAccount1 a = new ZijingOutAccount1();
            a.Show();

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            SalesOrderView1 p = new SalesOrderView1("1");
            p.Show();
        }
    }
}
