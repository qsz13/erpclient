﻿using Bicycle;
using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle_1
{
    public partial class ContactPersonCreate1 : Form
    {
        public ContactPersonCreate1(String a)
        {
            InitializeComponent();
            textBox1.Text = a;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ContactPersonCreate3 s = new ContactPersonCreate3();
            s.ShowDialog();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
  
            CustomerServiceClient client = new CustomerServiceClient();
            String id = textBox1.Text;
            bool result = client.ifContactPersonExist(id);
            if(result)
            {
                MessageBox.Show("联系人已存在");
            }
            else
            {
                ContactPersonCreate2 cpc2 = new ContactPersonCreate2(id);
                cpc2.ShowDialog();
                this.Close();
            }
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            CustomerServiceClient client = new CustomerServiceClient();
            bool result = client.ifexist(textBox1.Text);
            if(result)
            {
                CUSTOMER_INFORMATION c = client.getCustomerInfo(textBox1.Text);
                label2.Text = c.C_NAME;
            }
            else
            {
                label2.Text = "ID不存在";
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ContectPersonCreate1_Load(object sender, EventArgs e)
        {

        }
    }
}
