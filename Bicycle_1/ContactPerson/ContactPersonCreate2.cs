﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class ContactPersonCreate2 : Form
    {
        String customerID = null; 
        public ContactPersonCreate2(String a)
        {
            InitializeComponent();
            customerID = a;
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void ContectPersonCreate2_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String name = textBox1.Text;
            String sex = null;
            String telephone = textBox3.Text;
            String department = textBox4.Text;
            String description = textBox5.Text;
            if (comboBox1.SelectedIndex==0) sex = "M";
            else sex = "F";
            CustomerServiceClient client = new CustomerServiceClient();
            client.createContactPerson(customerID, name, sex, telephone, department, description);
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }


    }
}
