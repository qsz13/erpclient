﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Bicycle_1;

namespace Bicycle
{
    public partial class SalesOrderView2 : Form
    {
        public SalesOrderView2(string orderid)
        {
            InitializeComponent();
            MMServiceClient client = new MMServiceClient();


            SALES_ORDER so = client.getSalesOrder(orderid);


            label5.Text = so.SO_ORDERDATE.ToString();
            label8.Text = User.picktime.ToString();
            label6.Text = User.delivertime.ToString();
            label7.Text = User.paytime.ToString();
        }

        private void SalesOrderView2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
