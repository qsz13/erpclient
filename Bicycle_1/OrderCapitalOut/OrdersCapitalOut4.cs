﻿using BicycleMM;
using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class OrdersCapitalOut4 : Form
    {
        public string Company;
        public int sd = 0;
        int lin = 0;
        public OrdersCapitalOut4(string company,int ds)
        {
            InitializeComponent();
            sd = ds;
            Company = company;

            MMServiceClient fk = new MMServiceClient();
            List<VENDOR_INFORMATION> list = new List<VENDOR_INFORMATION>();
            list = fk.searchVenderID(Company).ToList();
            Dictionary<String, String> dic = new Dictionary<String, String>();

            int i = 0, j = 0;
            foreach (VENDOR_INFORMATION vi in list)
            {
                dic.Add(vi.VI_ID.ToString(), vi.VI_NAME);
                List<PURCHASE_ORDER> Vlist = new List<PURCHASE_ORDER>();
                Vlist = fk.getPOByVender(vi.VI_ID.ToString()).ToList();
                foreach (PURCHASE_ORDER temp in Vlist)
                {
//                    MessageBox.Show(temp.PO_ID + "     " + vi.VI_NAME);
                    dataGridView1.RowCount++;

                    dataGridView1.Rows[i].Cells[0].Value = temp.PO_ID.ToString();
                    dataGridView1.Rows[i].Cells[1].Value = vi.VI_NAME;
                    dataGridView1.Rows[i].Cells[2].Value = temp.PO_ORDERDATE.ToString();
                    dataGridView1.Rows[i].Cells[3].Value = temp.PO_TOTALPRICE.ToString();
                    i = i + 1;

                }
                lin = dataGridView1.RowCount;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int result = 0;
            for (int j = 0; j < lin; j++)
           {
               if (dataGridView1.Rows[j].Cells[4] .Value!= null)
               {
                   result = j;
               }
           }
            OrdersCapitalOut1 o = new OrdersCapitalOut1(dataGridView1.Rows[result].Cells[0].Value.ToString(), sd);
            this.Close();
            o.ShowDialog();
        }

        private void OrdersCapitalIn4_Load(object sender, EventArgs e)
        {

        }
    }
}
