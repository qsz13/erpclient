﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class OrdersCapitalOut3 : Form
    {
        int sd = 0;
        public OrdersCapitalOut3(int b)
        {
            InitializeComponent();
            sd = b;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OrdersCapitalOut4 oi4 = new OrdersCapitalOut4(textBox1.Text,sd);
            oi4.ShowDialog();
            this.Hide();
        }

        private void OrdersCapitalOut3_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
