﻿using Bicycle;
using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicycleMM
{
    public partial class OrdersCapitalOut1 : Form
    {
        int sd = 0;
        public OrdersCapitalOut1(String a,int b)
        {
            InitializeComponent();
            textBox1.Text = a;
            sd = b;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (sd == 1)
            {
                OrdersCapitalOut2 s = new OrdersCapitalOut2(textBox1.Text);
                s.ShowDialog();
            }
                if (sd == 2)
                {
                    PurchaseOrderManagement1 sB = new PurchaseOrderManagement1(textBox1.Text);
                    sB.ShowDialog();

                }
                if (sd == 3)
                {
                    Receive1 sR = new Receive1(textBox1.Text);
                    sR.ShowDialog();

                }
                if (sd == 4)
                {
                    Store1 ss = new Store1(textBox1.Text);
                    ss.ShowDialog();

                }

                 
            this.Close();
        }

        private void OrdersCapitalOut1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            OrdersCapitalOut3 s = new OrdersCapitalOut3(sd);
            s.ShowDialog();
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            MMServiceClient fk=new MMServiceClient();
            if (fk.ifPurchaseOrderExist(textBox1.Text))
            {
                PURCHASE_ORDER po = new PURCHASE_ORDER();
                po = fk.getPurchseOrder(textBox1.Text);
                VENDOR_INFORMATION vi = new VENDOR_INFORMATION();
                vi = fk.getVenderInformation(po.PO_VENDORID.ToString());
                label2.Text = vi.VI_NAME;
            }
            else
            {
                label2.Text = "订单ID不存在";
            }
        }
    }
}
