﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicycleMM
{
    public partial class OrdersCapitalOut2 : Form
    {
        Dictionary<String, decimal> ds = new Dictionary<String, decimal>();
 
        String id;
        public OrdersCapitalOut2(String a)
        {
            InitializeComponent();
            MMServiceClient client = new MMServiceClient();
            PURCHASE_ORDER po = new PURCHASE_ORDER();
            VENDOR_INFORMATION vo=new VENDOR_INFORMATION();
            id = a;
            po = client.getPurchseOrder(id);
            vo = client.getVenderInformation(po.PO_VENDORID.ToString());

            label8.Text = po.PO_VENDORID.ToString();
            label11.Text = vo.VI_NAME;
            label13.Text = id;
            label10.Text = vo.VI_ACCOUNT.ToString();
            label12.Text = po.PO_DOWNPAYMENT.ToString();
            label9.Text = po.PO_TOTALPRICE.ToString();
            List<ACCOUNT> alist = new List<ACCOUNT>();
            alist = client.getAccountInformation().ToList();

            for (int i = 0; i < alist.Count; i++)
            {
                ds.Add(alist[i].A_NUMBER.ToString(), Convert.ToDecimal(alist[i].A_BALANCE));
            }
            List<String> accountList=new List<String>();
            foreach(KeyValuePair<String,decimal> cc in ds)
            {
                accountList.Add(cc.Key);
            }
            comboBox1.DataSource = accountList;

        }

        private void OrdersCapitalOut2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String inaccount = textBox1.Text;
            String outaccount = comboBox1.SelectedItem.ToString();
            decimal need= Convert.ToDecimal(label9.Text);
            decimal amm = 0;
            foreach (KeyValuePair<String, decimal> cc in ds)
            {
                if (cc.Key == outaccount)
                {
                    amm = cc.Value;
                }
            }
        
            decimal outnum = Convert.ToDecimal(textBox3.Text);
            String discrib = textBox2.Text;
            DateTime time = DateTime.Now;
            MMServiceClient client = new MMServiceClient();
            client.createCapitalOut(time, inaccount, outaccount,"123", discrib, "278", id);
            client.updatePOPaidMoney(textBox3.Text, id);

            if (outnum + amm >= need)
            {
                client.updatePOAllPaidTime(time, id);
            }
            MessageBox.Show("出账成功");
            client.Close();
            this.Close();
                //从purchase order condition 读出值为全额付款的INT。更新purchase order 中的订单状态以及全额付款时间.
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
