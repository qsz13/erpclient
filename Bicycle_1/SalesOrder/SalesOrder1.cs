﻿using System;
using ERPServer;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class SalesOrder1 : Form
    {
        public SalesOrder1(String a)
        {
            InitializeComponent();
            textBox1.Text = a;
        }

        public SalesOrder1()
        {
            // TODO: Complete member initialization
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            SalesOrder2 so = new SalesOrder2(textBox1.Text);
            so.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            SalesOrder4 s = new SalesOrder4();
            s.ShowDialog();
            this.Close();
        }

        private void textBox1_TextChanged(object sender, System.EventArgs e)
        {
            CustomerServiceClient client = new CustomerServiceClient();
            bool result = client.ifexist(textBox1.Text);
            if (result)
            {
                CUSTOMER_INFORMATION c = client.getCustomerInfo(textBox1.Text);
                label2.Text = c.C_NAME;
            }
            else
            {
                label2.Text = "ID不存在";
            }
        }
    }
}
