﻿using Bicycle_1;
using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class SalesOrder2 : Form
    {
        public string CusId;
        private int totalPrice = 0;
        private Dictionary<int, String> dicDeliveryPriority;
        private Dictionary<int, String> dicShipCondition;
        private Dictionary<int, String> dicDistributionChannel;

        private CUSTOMER_INFORMATION customer_info;
        private void getDeliveryPriority()
        {
            SDServiceClient SDClient = new SDServiceClient();
            dicDeliveryPriority = SDClient.getAllDeliveryPriority();
            SDClient.Close();
        }

        private void getShipCondition()
        {
            SDServiceClient SDClient = new SDServiceClient();
            dicShipCondition = SDClient.getAllShipCondition();
            SDClient.Close();
        }

        private void getDistributionChannel()
        {
            SDServiceClient SDClient = new SDServiceClient();
            dicDistributionChannel = SDClient.getAllDistributionChannel();
            SDClient.Close();
        }


        private void getCustomerInformation()
        {
            CustomerServiceClient customerClient = new CustomerServiceClient();
            customer_info = customerClient.getCustomerInfo(CusId);
            customerClient.Close();
        }


        public SalesOrder2(String cusid)
        {
            InitializeComponent();
            CusId = cusid;
            getDeliveryPriority();
            getShipCondition();
            getDistributionChannel();
            getCustomerInformation();
            int priority = (int)customer_info.C_DELIVPRIO;
            String priorityString = dicDeliveryPriority[priority];
            String company = customer_info.C_NAME;
            int condition = (int)customer_info.C_SHIPCON;
            String conditionString = dicShipCondition[condition];

            int code = (int)customer_info.C_POSTCODE;
            int channel = (int)customer_info.C_DCHANNEL;
            String channelString = dicDistributionChannel[channel] ;

            label8.Text = CusId.ToString();
            label14.Text = priorityString;
            label9.Text = company;
            label11.Text = conditionString;
            label13.Text = code.ToString();
            label10.Text = channelString;

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void SalesOrder2_Load(object sender, EventArgs e)
        {

        }

        private String getMaterialName(String id)
        {
            MMServiceClient mmClient = new MMServiceClient();
            MATERIAL m = mmClient.getMaterial(id);
            mmClient.Close();
            return m.M_NAME;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (button1.Text == "提交")
            {
                bool able = true;
                for (int j = 0; j < dataGridView1.RowCount-1; j++)
                {
                    String mid = dataGridView1.Rows[j].Cells[0].Value.ToString();
                    MMServiceClient mmClient = new MMServiceClient();
                    bool result = mmClient.ifMaterialExist(mid);
                    mmClient.Close();
                    if ( !result )//判断是否存在
                    {
                        able = false;
                    }
                }
                if (able == true)
                {
                    button1.Text = "确认";
                    label12.Text = "确认订单";
                    MessageBox.Show("已提交，请确认");
                    
                    for (int j = 0; j < dataGridView1.RowCount - 1; j++)
                    {
                        String name = getMaterialName(dataGridView1.Rows[j].Cells[0].Value.ToString());
                        dataGridView1.Rows[j].Cells[1].Value = name;//物料名称对应编号
                        totalPrice += Convert.ToInt32(dataGridView1.Rows[j].Cells[2].Value) * Convert.ToInt32(dataGridView1.Rows[j].Cells[3].Value);
                    }
                    label19.Text = totalPrice.ToString();
                }
                else
                {
                    MessageBox.Show("请确保输入存在的物料");
                }
            }
            else
            {

                SDServiceClient SDClient = new SDServiceClient();
                DateTime deadline = dateTimePicker1.Value.Date;
                //SDClient.createSalesOrder(this.CusId, deadline, User.id, textBox1.Text, totalPrice);
                int orderID = SDClient.createSalesOrder(this.CusId, deadline, "124", textBox1.Text, totalPrice);
                MessageBox.Show("销售订单编号："+orderID.ToString());
                
                //将订单信息输入数据库 有priority company condition code channel等等
                for (int j = 0; j < dataGridView1.RowCount - 1; j++)
                {
                    String materialOrder = dataGridView1.Rows[j].Cells[0].Value.ToString();
                    String amount = dataGridView1.Rows[j].Cells[2].Value.ToString();
                    String itemPrice = dataGridView1.Rows[j].Cells[3].Value.ToString();
                    SDClient.createDeliveryItem(orderID.ToString(), materialOrder, amount, itemPrice);
                    //将dataGridView1.Rows[j].Cells[0].Value 及2、3的值输入数据库并建立联系
                }
                SDClient.Close();
                this.Close();
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
