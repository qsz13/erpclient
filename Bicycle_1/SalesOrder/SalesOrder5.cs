﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class SalesOrder5 : Form
    {

        public String query;
        private List<String> resultCompanyList = new List<String>();


        public SalesOrder5(String query)
        {
            InitializeComponent();
            this.query = query;
            CustomerServiceClient client = new CustomerServiceClient();
            Dictionary<int,String> result = client.searchCustomer(query);
            List<String> temp = new List<String>();
            foreach (KeyValuePair<int, String> a in result)
            {
                temp.Add(a.Key+"                 "+a.Value);
                resultCompanyList.Add(a.Key.ToString());
            }
            listBox1.DataSource = temp;
        }

        private void SalesOrder5_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            String slectedID = resultCompanyList[index];
            SalesOrder1 s = new SalesOrder1(slectedID);
            s.ShowDialog();
            this.Close();
        }
    }
}
