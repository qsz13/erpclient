﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bicycle_1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            label2.Text = User.name;
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            /*直接放到treeview的函数体里面 注释掉的部分为未实现功能*/



            if (e.Node.Text.Equals("创建客户"))
            {
            }
            else if (e.Node.Text.Equals("客户信息管理"))
            {
            }
            else if (e.Node.Text.Equals("创建联系人"))
            {
            }
            else if (e.Node.Text.Equals("联系人信息管理"))
            {
            }
            else if (e.Node.Text.Equals("创建销售订单"))
            {
            }
            else if (e.Node.Text.Equals("管理销售订单"))
            {
            }
            else if (e.Node.Text.Equals("查看销售订单"))
            {
            }
            else if (e.Node.Text.Equals("创建供应商"))
            {
            }
            else if (e.Node.Text.Equals("供应商信息管理"))
            {
            }
            else if (e.Node.Text.Equals("创建物料"))
            {
            }
            else if (e.Node.Text.Equals("管理物料"))
            {
            }
            else if (e.Node.Text.Equals("创建物料"))
            {
            }
            else if (e.Node.Text.Equals("创建采购订单"))
            {
            }
            else if (e.Node.Text.Equals("管理采购订单"))
            {
            }
            else if (e.Node.Text.Equals("查看采购订单"))
            {
            }
            else if (e.Node.Text.Equals("订单入库"))
            {
            }
            else if (e.Node.Text.Equals("无单入库"))
            {
            }
            else if (e.Node.Text.Equals("订单拣料"))
            {
            }
            else if (e.Node.Text.Equals("无单拣料"))
            {
            }
            else if (e.Node.Text.Equals("收货"))
            {
            }
            else if (e.Node.Text.Equals("创建车间"))
            {
            }
            else if (e.Node.Text.Equals("管理车间信息"))
            {
            }
            else if (e.Node.Text.Equals("创建生产计划"))
            {
            }
            else if (e.Node.Text.Equals("管理生产计划"))
            {
            }
            else if (e.Node.Text.Equals("查看生产计划"))
            {
            }
            else if (e.Node.Text.Equals("生产过程"))
            {
            }
            else if (e.Node.Text.Equals("销售订单入账"))
            {
            }
            else if (e.Node.Text.Equals("资金入账"))
            {
            }
            else if (e.Node.Text.Equals("采购订单出账"))
            {
            }
            else if (e.Node.Text.Equals("资金出账"))
            {
            }
            else if (e.Node.Text.Equals("查看账户"))
            {
            }
            else if (e.Node.Text.Equals("查看财务记录"))
            {
            }
            else if (e.Node.Text.Equals("员工信息管理"))
            {
            }
            else if (e.Node.Text.Equals("创建公司账户"))
            {
            }
            else if (e.Node.Text.Equals("管理公司账户"))
            {
            }
            else if (e.Node.Text.Equals("创建物料组"))
            {
            }
            else if (e.Node.Text.Equals("管理物料组"))
            {
            }
            else if (e.Node.Text.Equals("创建分销渠道"))
            {
            }
            else if (e.Node.Text.Equals("管理分销渠道"))
            {
            }
            else if (e.Node.Text.Equals("创建交货优先级"))
            {
            }
            else if (e.Node.Text.Equals("管理交货优先级"))
            {
            }
            else if (e.Node.Text.Equals("创建发运条件"))
            {
            }
            else if (e.Node.Text.Equals("管理发运条件"))
            {
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void LogoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void HelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 f=new Form1();
            f.Show();
        }



    }
}
