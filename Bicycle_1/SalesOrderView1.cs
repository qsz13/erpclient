﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class SalesOrderView1 : Form
    {
         public string SO_ID;
         List<String> materialNameList = new List<String>();
        public SalesOrderView1(string orderid)
        {
            InitializeComponent();
            label13.Text = orderid;
            SO_ID = orderid;

            MMServiceClient client = new MMServiceClient();
            CustomerServiceClient cclient = new CustomerServiceClient();


            SALES_ORDER so = client.getSalesOrder(orderid);
            CUSTOMER_INFORMATION c = cclient.getCustomerInfo(so.SO_CUSID.ToString());

            string cusid = c.C_ID.ToString();
            string cusname = c.C_NAME;

            string cusaccount = c.C_BANKACCOUNT;
            string payment = so.SO_DOWNPAYMENT.ToString();
            string totalprice = so.SO_TOTALPRICE.ToString();
            string time=so.SO_ORDERDATE.ToString();

            label8.Text = cusid;
            label11.Text = cusname;
            label13.Text = orderid;
            label10.Text = cusaccount;
            label12.Text = payment;
            label9.Text = totalprice;
            label15.Text=time;


            SDServiceClient sdClient = new SDServiceClient();

            SALES_ORDER sso = sdClient.getSalesOrder(orderid);
            Nullable<decimal> state = sdClient.getSalesOrder(orderid).SO_SORDERSTATE;
            string con = null;
            if (state != null)
            {
                con = state.ToString();
            }
            label17.Text = con;




            SO_ID = orderid;
            SDServiceClient sds = new SDServiceClient();
            CustomerServiceClient cs = new CustomerServiceClient();
            MMServiceClient mms = new MMServiceClient();
            List<DELIVERY_ITEM> deliveryItemList = new List<DELIVERY_ITEM>();

            SALES_ORDER salesOrder = sds.getSalesOrder(SO_ID);
            String cusID = salesOrder.SO_CUSID.ToString();
            CUSTOMER_INFORMATION cusInf = cs.getCustomerInfo(cusID);
            String cusName = cusInf.C_NAME.ToString();
            String delivprio = cusInf.C_DELIVPRIO.ToString();
            String channel = cusInf.C_DCHANNEL.ToString();
            String shipCondition = cusInf.C_SHIPCON.ToString();





            deliveryItemList = sds.getDeliveryItemList(SO_ID).ToList();


            for (int i = 0; i < deliveryItemList.Count; i++)
            {
                materialNameList.Add(mms.getMaterial(deliveryItemList[i].DI_MATERIALID.ToString()).M_NAME.ToString());
              //  DI_ID.Add(deliveryItemList[i].DI_ID.ToString());
              //  M_ID.Add(deliveryItemList[i].DI_MATERIALID.ToString());
            }


            for (int i = 0; i < deliveryItemList.Count; i++)
            {
                dataGridView1.RowCount = i + 2;
                //   String materialID = purchaseItem_list[i].PI_MATERIALID.ToString();
                dataGridView1.Rows[i].Cells[0].Value = deliveryItemList[i].DI_MATERIALID.ToString();
                dataGridView1.Rows[i].Cells[1].Value = materialNameList[i];
                dataGridView1.Rows[i].Cells[2].Value = deliveryItemList[i].DI_PREAMOUNT.ToString();
                dataGridView1.Rows[i].Cells[3].Value = mms.getMaterial(deliveryItemList[i].DI_MATERIALID.ToString()).M_PRICE.ToString();

            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void SalesOrderView1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SalesOrderView2 sov2 = new SalesOrderView2(SO_ID);
            sov2.ShowDialog();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
