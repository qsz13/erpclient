﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class SalesOrderManagement1 : Form
    {
        public SalesOrderManagement1(string orderid)
        {
            InitializeComponent();
            label13.Text = orderid;

            MMServiceClient client = new MMServiceClient();
            CustomerServiceClient cclient = new CustomerServiceClient();


            SALES_ORDER so = client.getSalesOrder(orderid);
            CUSTOMER_INFORMATION c = cclient.getCustomerInfo(so.SO_CUSID.ToString());

            string cusid = c.C_ID.ToString();
            string cusname = c.C_NAME;

            string cusaccount = c.C_BANKACCOUNT;
            string payment = so.SO_DOWNPAYMENT.ToString();
            string totalprice = so.SO_TOTALPRICE.ToString();

            label8.Text = cusid;
            label11.Text = cusname;
            label13.Text = orderid;
            label10.Text = cusaccount;
            label12.Text = payment;
            label9.Text = totalprice;


            SDServiceClient sdClient = new SDServiceClient();

            SALES_ORDER sso = sdClient.getSalesOrder(orderid);
            Nullable<decimal> state = sdClient.getSalesOrder(orderid).SO_SORDERSTATE;
            string con = null;
            if (state != null)
            {
                con = state.ToString();
            }
            label15.Text = con;

        }

        private void SalesOrderManagement1_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SDServiceClient sdClient = new SDServiceClient();

            SALES_ORDER so = sdClient.getSalesOrder(label13.Text);
            Nullable<decimal> state = sdClient.getSalesOrder(label13.Text).SO_SORDERSTATE;
            string con = null;
            if (state != null)
            {
                con = state.ToString();
            }
            if (con == "4")
            {
                con = "5";
                MessageBox.Show("订单完成");
                sdClient.updateSalesOrder(label13.Text, Convert.ToInt32(con));
                //更新订单状态
            }
            else
            {
                MessageBox.Show("订单未付全款");
            }            
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SDServiceClient sdClient = new SDServiceClient();
            MessageBox.Show("订单已被取消");
            sdClient.updateSalesOrder(label13.Text, Convert.ToInt32("6"));
            //更新订单状态
            this.Close();
        }
    }
}
