﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class SalesOrderManagement2 : Form
    {
        public SalesOrderManagement2(string orderid)
        {
            InitializeComponent();
            textBox1.Text = orderid;
        }

        private void SalesOrderManagement2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SalesOrderManagement1 som1 = new SalesOrderManagement1(textBox1.Text);
            som1.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
