﻿using Bicycle_1;
using BicycleFI;
using BicycleMM;
using BicyclePP;
using Management;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Text.Equals("创建客户"))
            {
                CreateCustomerForm ccf = new CreateCustomerForm();
                ccf.ShowDialog();
            }
            else if (e.Node.Text.Equals("客户信息管理"))
            {

            }
            else if (e.Node.Text.Equals("创建联系人"))
            {
                ContactPersonCreate1 cpc = new ContactPersonCreate1(null);
                cpc.ShowDialog();
                
            }
            else if (e.Node.Text.Equals("联系人信息管理"))
            {
            }
            else if (e.Node.Text.Equals("创建销售订单"))
            {
                SalesOrder1 so =new SalesOrder1(null);
                so.ShowDialog();

            }
            else if (e.Node.Text.Equals("管理销售订单"))

            {
                OrdersCapitalIn2 oc = new OrdersCapitalIn2(null, 4);
                oc.ShowDialog();
            }
            else if (e.Node.Text.Equals("查看销售订单"))
            {
                OrdersCapitalIn2 oc = new OrdersCapitalIn2(null, 5);
                oc.ShowDialog();
            }
            else if (e.Node.Text.Equals("创建供应商"))
            {
                VendorCreate1 vc = new VendorCreate1();
                vc.ShowDialog();
            }
            else if (e.Node.Text.Equals("供应商信息管理"))
            {
            }
            else if (e.Node.Text.Equals("创建物料"))
            {
                MaterialCreate1 mc = new MaterialCreate1();
                mc.ShowDialog();
            }
            else if (e.Node.Text.Equals("管理物料"))
            {
            }
            else if (e.Node.Text.Equals("发货"))
            {
                OrdersCapitalIn2 oc = new OrdersCapitalIn2(null, 3);
                oc.ShowDialog();
            }
            else if (e.Node.Text.Equals("创建物料"))
            {
                MaterialCreate1 mc = new MaterialCreate1();
                mc.ShowDialog();
            }
            else if (e.Node.Text.Equals("创建采购订单"))
            {
                PurchaseOrderCreate2 poc = new PurchaseOrderCreate2();
                poc.ShowDialog();
            
            }
            else if (e.Node.Text.Equals("管理采购订单"))
            {
//                User.renger = "M";
                OrdersCapitalOut1 oco = new OrdersCapitalOut1(null, 2);
                oco.ShowDialog();
            }
            else if (e.Node.Text.Equals("查看采购订单"))
            {
  //              User.renger = "F";
                OrdersCapitalOut1 oco = new OrdersCapitalOut1(null, 2);
                oco.ShowDialog();

            }
            else if (e.Node.Text.Equals("订单入库"))
            {
                OrdersCapitalOut1 oco = new OrdersCapitalOut1(null, 4);
                oco.ShowDialog();

            }
            else if (e.Node.Text.Equals("无单入库"))
            {
                NoOrderStore1 pu1 = new NoOrderStore1("");
                pu1.ShowDialog();
            }
            else if (e.Node.Text.Equals("订单拣料"))
            {
                OrdersCapitalIn2 oc = new OrdersCapitalIn2(null, 2);
                oc.ShowDialog();
                //未写完
            }
            else if (e.Node.Text.Equals("无单拣料"))
            {
                NoOrderPickUp1 pu1 = new NoOrderPickUp1("");
                pu1.ShowDialog();
            }
            else if (e.Node.Text.Equals("收货"))
            {
                OrdersCapitalOut1 oco = new OrdersCapitalOut1(null, 3);
                oco.ShowDialog();

            }
            else if (e.Node.Text.Equals("创建车间"))
            {
                WorkshopCreate1 wc = new WorkshopCreate1();
                wc.ShowDialog();
            }
            else if (e.Node.Text.Equals("管理车间信息"))
            {
            }
            else if (e.Node.Text.Equals("创建生产计划"))
            {
                ProductionPlanCreate1 ppc1 = new ProductionPlanCreate1();
                ppc1.ShowDialog();
            }
            else if (e.Node.Text.Equals("管理生产计划"))
            {
                ProductionPlanManagement1 ppm1 = new ProductionPlanManagement1("", 1);
                ppm1.ShowDialog();
            }
            else if (e.Node.Text.Equals("查看生产计划"))
            {
                ProductionPlanManagement1 ppm = new ProductionPlanManagement1("", 2);
                ppm.ShowDialog();
            }
            else if (e.Node.Text.Equals("生产过程"))
            {
            }
            else if (e.Node.Text.Equals("销售订单入账"))
            {
                OrdersCapitalIn2 oc = new OrdersCapitalIn2(null, 1);
                oc.ShowDialog();
            }
            else if (e.Node.Text.Equals("资金入账"))
            {
                ZijingIntoAccount1 zji = new ZijingIntoAccount1();
                zji.ShowDialog();
            }
            else if (e.Node.Text.Equals("采购订单出账"))
            {
                OrdersCapitalOut1 oco = new OrdersCapitalOut1(null, 1);
                oco.ShowDialog();
            }
            else if (e.Node.Text.Equals("资金出账"))
            {
                ZijingOutAccount1 zjo = new ZijingOutAccount1();
                zjo.ShowDialog();

            }
            else if (e.Node.Text.Equals("查看账户"))
            {
                AccountView1 av = new AccountView1();
                av.ShowDialog();
            }
            else if (e.Node.Text.Equals("查看财务记录"))
            {
                RecordView1 rv = new RecordView1();
                rv.ShowDialog();
            }
            else if (e.Node.Text.Equals("员工信息管理"))
            {
                if (User.id.Equals("1"))
                {
                    StaffManagement sm = new StaffManagement();
                    sm.ShowDialog();
                }
                else
                    MessageBox.Show("您的权限不够");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
 //           CustomerCreate1 a = new CustomerCreate1();
 //           a.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
    //        OrdersCapitalIn1 oci1 = new OrdersCapitalIn1();
     //       oci1.ShowDialog();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
     //       PickUp1 pu1 = new PickUp1();
      //      pu1.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
        //    Post1 p1 = new Post1();
        //    p1.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
        //    SalesOrder1 so1 = new SalesOrder1();
        //    so1.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
       //     SalesOrderManagement1 som1 = new SalesOrderManagement1();
       //     som1.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
       //     SalesOrderView1 sov1 = new SalesOrderView1();
       //     sov1.ShowDialog();
        }

        private void 帮助ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("请联系工作人员获取进一步信息。");
        }

        private void 帮助ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 a = new Form1();
            a.Show();

        }

        private void 注销ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
