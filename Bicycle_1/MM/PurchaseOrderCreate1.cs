﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;
using Bicycle_1;

namespace BicycleMM
{
    public partial class PurchaseOrderCreate1 : Form
    {
        public String venderID;

        public PurchaseOrderCreate1()
        {
            InitializeComponent();

        }

        public PurchaseOrderCreate1(String venderID)
        {
            InitializeComponent();
            this.venderID = venderID;

            MMServiceClient mms = new MMServiceClient();
            VENDOR_INFORMATION v = mms.getVenderInformation(venderID);

            this.label8.Text = venderID;
            this.label11.Text = v.VI_NAME;
            this.label13.Text = v.VI_COUNTRY;
            this.label10.Text = v.VI_ADDRESS;
            this.label12.Text = v.VI_CITY;
            this.label9.Text = v.VI_POSTCODE.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PurchaseOrderCreate1_Load_1(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (button1.Text == "提交")
            {
                bool able = true;
                for (int j = 0; j < dataGridView1.RowCount-1; j++)
                {
                    MMServiceClient mms = new MMServiceClient();
                    String materialID = (String)dataGridView1.Rows[j].Cells[0].Value;
                    able = mms.ifMaterialExist(materialID);
//                    MessageBox.Show(able.ToString());
                    if (!able)
                    {
                        int i = j + 1;
                        MessageBox.Show("第" + 1 + "行确保输入存在的物料");
                    }
                    dataGridView1.Rows[j].Cells[1].Value = mms.getMaterial(materialID).M_NAME.ToString();
                }
                if (able == true)
                {
                    button1.Text = "确认订单";
                    MessageBox.Show("已提交，请确认");
                    int sum = 0;
                    for (int j = 0; j < dataGridView1.RowCount-1; j++)
                    {                      
                        sum += Convert.ToInt32(dataGridView1.Rows[j].Cells[2].Value) * Convert.ToInt32(dataGridView1.Rows[j].Cells[3].Value);
                    }
                    textBox2.Text = sum.ToString();
                }

            }
            else
            {

                for (int j = 0; j < dataGridView1.RowCount; j++)
                {
                   // MMServiceClient mms = new MMServiceClient();
                   // mms.createPurchaseOrder(venderID, textBox1.ToString(), textBox2.ToString(),User.id.ToString());
                }

                this.Hide();

            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }
    }
}