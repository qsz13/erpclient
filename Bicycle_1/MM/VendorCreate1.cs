﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicycleMM
{
    public partial class VendorCreate1 : Form
    {
        public VendorCreate1()
        {
            InitializeComponent();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String name = textBox1.Text;
            String country = textBox2.Text;
            String city = textBox3.Text;
            String address = textBox4.Text;
            String code = textBox5.Text;
            String account = textBox6.Text;
            String telephone = textBox7.Text;
            MMServiceClient fk = new MMServiceClient();
            fk.creatVenderInformation(name,country,city,address,Convert.ToInt32(code),account,telephone);
            MessageBox.Show("供应商创建成功");
            this.Close();
        }
    }
}
