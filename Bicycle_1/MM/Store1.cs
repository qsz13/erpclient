﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicycleMM
{
    public partial class Store1 : Form
    {
           
        public String purchaseOrder_id;
        List<String> PI_ID = new List<String>();
        public Store1(String PO_id)
        {
            InitializeComponent();
            this.purchaseOrder_id = PO_id;
            MMServiceClient mms = new MMServiceClient();
            PURCHASE_ORDER purchaseOrder = mms.getPurchseOrder(this.purchaseOrder_id);

            String venderID = purchaseOrder.PO_VENDORID.ToString();
            label8.Text = venderID;
            label11.Text = mms.getVenderInformation(venderID).VI_NAME;
            label13.Text = this.purchaseOrder_id;

            List<PURCHASE_ITEM> purchaseItem_list = new List<PURCHASE_ITEM>();
            purchaseItem_list = mms.getPurchaseItem(this.purchaseOrder_id).ToList();


            foreach (PURCHASE_ITEM pi in purchaseItem_list)
            {
                PI_ID.Add(pi.PI_ID.ToString());
            }

            for (int i = 0; i < purchaseItem_list.Count; i++)
            {
                dataGridView1.RowCount = i + 2;
                String materialID = purchaseItem_list[i].PI_MATERIALID.ToString();
                dataGridView1.Rows[i].Cells[0].Value = materialID;
                dataGridView1.Rows[i].Cells[1].Value = mms.getMaterial(materialID).M_NAME;
                dataGridView1.Rows[i].Cells[2].Value = purchaseItem_list[i].PI_PREAMOUNT;
                dataGridView1.Rows[i].Cells[3].Value = purchaseItem_list[i].PI_STOREAMOUNT;

            }

        }

        private void Store1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MMServiceClient mms = new MMServiceClient();
            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                String PI_id = PI_ID[i];
                if (dataGridView1.Rows[i].Cells[4].Value == null)
                {
                    MessageBox.Show("请输入本次入库数量");
                }
                else
                {
                    
                    mms.updateStoreAmount(PI_id, dataGridView1.Rows[i].Cells[4].Value.ToString());
                }
            }

            //获取订单物料信息再显示
            List<PURCHASE_ITEM> purchaseItem_list = new List<PURCHASE_ITEM>();
            purchaseItem_list = mms.getPurchaseItem(this.purchaseOrder_id).ToList();
            for (int i = 0; i < purchaseItem_list.Count; i++)
            {
                dataGridView1.RowCount = i + 2;
                String materialID = purchaseItem_list[i].PI_MATERIALID.ToString();
                dataGridView1.Rows[i].Cells[0].Value = materialID;
                dataGridView1.Rows[i].Cells[1].Value = mms.getMaterial(materialID).M_NAME;
                dataGridView1.Rows[i].Cells[2].Value = purchaseItem_list[i].PI_PREAMOUNT;
                dataGridView1.Rows[i].Cells[3].Value = purchaseItem_list[i].PI_STOREAMOUNT;
                dataGridView1.Rows[i].Cells[4].Value = "0";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }
    }
}
