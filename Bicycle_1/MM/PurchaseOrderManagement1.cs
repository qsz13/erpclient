﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicycleMM
{
    public partial class PurchaseOrderManagement1 : Form
    {
        public PurchaseOrderManagement1(String b)
        {
            InitializeComponent();
            String id = b;
            MMServiceClient fk = new MMServiceClient();
            PURCHASE_ORDER po = new PURCHASE_ORDER();
            VENDOR_INFORMATION vo = new VENDOR_INFORMATION();
            po = fk.getPurchseOrder(id);
            vo = fk.getVenderInformation(po.PO_VENDORID.ToString());
            label8.Text = po.PO_VENDORID.ToString();
            label11.Text = vo.VI_NAME;
            label13.Text = id;
            label10.Text = vo.VI_ACCOUNT;
            label12.Text = po.PO_DOWNPAYMENT.ToString();
            label9.Text = po.PO_TOTALPRICE.ToString();
            label15.Text = po.PO_PORDERSTATE.ToString();
            //需要从订单状态对应表中读出状态ID所对应的中文名称
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("订单已经完成。");
            //需要从订单状态对应表中读出完成状态所对应的int
            //将purchase order 表中的订单状态栏update成int
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("订单取消成功。");

            //需要从订单状态对应表中读出取消状态所对应的int
            //将purchase order 表中的订单状态栏update成int

        }

        private void PurchaseOrderManagement1_Load(object sender, EventArgs e)
        {

        }
    }
}
