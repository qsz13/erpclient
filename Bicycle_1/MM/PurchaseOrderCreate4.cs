﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicycleMM
{


    public partial class PurchaseOrderCreate4 : Form
    {
        public String query;
        private List<VENDOR_INFORMATION> resultVendorList = new List<VENDOR_INFORMATION>();
        Dictionary<int, String> result = new Dictionary<int, string>();
        List<String> vendorID = new List<String>();

        public PurchaseOrderCreate4(String query)
        {
            InitializeComponent();
            this.query = query;

            /****************数据库提供函数***********************
                  供应商名字搜索函数
                *****************************************************/
            MMServiceClient mms = new MMServiceClient();
            resultVendorList = mms.searchVenderID(query).ToList() ;
           
            List<String> temp = new List<String>();
            foreach (VENDOR_INFORMATION v in resultVendorList)
            {
                result.Add(Convert.ToInt32(v.VI_ID), v.VI_NAME);
                vendorID.Add(v.VI_ID.ToString());
            }
        
            foreach (KeyValuePair<int, String> a in result)
            {
                temp.Add(a.Key + "                 " + a.Value);
                           
            }
            listBox1.DataSource = temp;

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Hide();
            int index = listBox1.SelectedIndex;
            String slectedID = vendorID[index];
            PurchaseOrderCreate2 s = new PurchaseOrderCreate2(slectedID);
            s.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            int index = listBox1.SelectedIndex;
            String slectedID = vendorID[index];
            PurchaseOrderCreate2 s = new PurchaseOrderCreate2(slectedID);
            s.ShowDialog();
        }


    }
}
