﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace BicycleMM
{
    public partial class MaterialCreate1 : Form
    {
        public MaterialCreate1()
        {
            InitializeComponent();


            /****************数据库提供函数***********************
               List<String>   getMaterialGroupNo(); 
             *****************************************************/
            MMServiceClient mms = new MMServiceClient();
           List<String> MaterialGroupID = new List<String>();
           MaterialGroupID = mms.getMaterialGroupNo().ToList();
           comboBox1.DataSource = MaterialGroupID;        
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            
            String m_MaterialID = textBox1.Text;
            String m_Name = textBox2.Text;
            String m_MGID = (String)comboBox1.SelectedItem;
            String m_Cost = textBox5.Text;
            String m_Price = textBox6.Text;
            String m_Description = textBox3.Text;

            MMServiceClient mms = new MMServiceClient();
            if (mms.ifMaterialExist(m_MaterialID))
            {
                MessageBox.Show("物料已经存在");
            }
            else
            {
                mms.createMaterial(m_MaterialID, m_Name,m_MGID, m_Description, m_Cost, m_Price, "123");
                MessageBox.Show("物料储存成功");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }




    }
}
