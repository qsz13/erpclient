﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicycleMM
{
    public partial class PurchaseOrderCreate2 : Form
    {

        public bool result = false; 

        public PurchaseOrderCreate2(String vendorID)
        {
            InitializeComponent();
            textBox1.Text = vendorID;

        }

        public PurchaseOrderCreate2()
        {
            InitializeComponent();


        }

        private void PurchaseOrderCreate2_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            /****************数据库提供函数***********************
                   bool   ifexist(textBox1.text)   参数：供应商ID 返回：是否存在
            *****************************************************/

            MMServiceClient mms = new MMServiceClient();
            result = mms.ifVenderExist(textBox1.Text);        
            if (result)
            {
                VENDOR_INFORMATION v = mms.getVenderInformation(textBox1.Text);
                label2.Text = v.VI_NAME;
            }
            else
            {
                label2.Text = "ID不存在";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (result == true)
            {
                this.Hide();
                PurchaseOrderCreate1 s = new PurchaseOrderCreate1(textBox1.Text);
                s.ShowDialog();
            }
            else
            {
                MessageBox.Show("请输入正确供应商ID");
            }
                
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            PurchaseOrderCreate3 s = new PurchaseOrderCreate3();
            s.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
