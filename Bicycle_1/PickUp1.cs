﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;
using Bicycle_1;

namespace Bicycle
{
    public partial class PickUp1 : Form
    {
        public String SO_ID;
        List<String> DI_ID = new List<String>();
        List<String> M_ID = new List<String>();       
        List<String> materialNameList = new List<String>();

        public PickUp1(string orderid)
        {
            InitializeComponent();
            SO_ID = orderid;
            SDServiceClient sds = new SDServiceClient();
            CustomerServiceClient cs = new CustomerServiceClient();
            MMServiceClient mms = new MMServiceClient();
            List<DELIVERY_ITEM> deliveryItemList = new List<DELIVERY_ITEM>();

            SALES_ORDER salesOrder = sds.getSalesOrder(SO_ID);
            String cusID = salesOrder.SO_CUSID.ToString();
            CUSTOMER_INFORMATION cusInf = cs.getCustomerInfo(cusID);
            String cusName = cusInf.C_NAME.ToString();
            String delivprio = cusInf.C_DELIVPRIO.ToString();
            String channel = cusInf.C_DCHANNEL.ToString();
            String shipCondition = cusInf.C_SHIPCON.ToString();
            
            label8.Text = cusID;
            label11.Text = cusName;
            label13.Text = SO_ID;
            label10.Text = delivprio;
            label12.Text = channel;
            label9.Text = shipCondition;


          
            deliveryItemList   = sds.getDeliveryItemList(SO_ID).ToList();
            

            for(int i = 0 ; i < deliveryItemList.Count; i++)
            {
                materialNameList.Add(mms.getMaterial(deliveryItemList[i].DI_MATERIALID.ToString()).M_NAME.ToString());
                DI_ID.Add(deliveryItemList[i].DI_ID.ToString());
                M_ID.Add(deliveryItemList[i].DI_MATERIALID.ToString());
            }


            for (int i = 0; i < deliveryItemList.Count; i++)
            {
                dataGridView1.RowCount = i + 2;
             //   String materialID = purchaseItem_list[i].PI_MATERIALID.ToString();
                dataGridView1.Rows[i].Cells[0].Value = deliveryItemList[i].DI_MATERIALID.ToString();
                dataGridView1.Rows[i].Cells[1].Value = materialNameList[i];
                dataGridView1.Rows[i].Cells[2].Value = deliveryItemList[i].DI_PREAMOUNT.ToString();
                dataGridView1.Rows[i].Cells[3].Value = deliveryItemList[i].DI_PICKAMOUNT.ToString();

            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            SDServiceClient sds = new SDServiceClient();
            List<DELIVERY_ITEM> deliveryItemList = new List<DELIVERY_ITEM>();
            deliveryItemList = sds.getDeliveryItemList(SO_ID).ToList();

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                String DI_id = DI_ID[i];
                String M_id = M_ID[i];
                if (dataGridView1.Rows[i].Cells[4].Value.ToString() == null)
                {
                    MessageBox.Show("请输入本次收货数量");
                }
                else
                {
                    sds.updateDeliveryItemAmount(DI_id, dataGridView1.Rows[i].Cells[4].Value.ToString());
                    sds.updateMaterial(M_id, dataGridView1.Rows[i].Cells[4].Value.ToString());
                }
            }


            for (int i = 0; i < deliveryItemList.Count; i++)
            {
                dataGridView1.RowCount = i + 2;
                //   String materialID = purchaseItem_list[i].PI_MATERIALID.ToString();
                dataGridView1.Rows[i].Cells[0].Value = deliveryItemList[i].DI_MATERIALID.ToString();
                dataGridView1.Rows[i].Cells[1].Value = materialNameList[i];
                dataGridView1.Rows[i].Cells[2].Value = deliveryItemList[i].DI_PREAMOUNT.ToString();
                dataGridView1.Rows[i].Cells[3].Value = deliveryItemList[i].DI_PICKAMOUNT.ToString();
                dataGridView1.Rows[i].Cells[4].Value = "0";
            }

            bool en = true;
            for (int j = 0; j < dataGridView1.RowCount - 1;j++ )
            {
                if (Convert.ToDecimal(dataGridView1.Rows[j].Cells[3].Value) < Convert.ToDecimal(dataGridView1.Rows[j].Cells[2].Value))
                {
                    en = false;
                }
            }
            if (en == true)
            {
                SDServiceClient sdClient = new SDServiceClient();

                    MessageBox.Show("拣料完成");
                    User.picktime = DateTime.Now;
                    sdClient.updateSalesOrder(label13.Text, 2);
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void PickUp1_Load(object sender, EventArgs e)
        {

        }
    }
}
