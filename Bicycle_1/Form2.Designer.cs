﻿namespace Bicycle_1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("客户");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("联系人");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("销售订单");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("销售", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("供应商");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("物料");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("采购订单");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("入库");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("拣料");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("收货");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("发货");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("物料", new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11});
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("车间");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("生产计划");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("生产过程");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("生产", new System.Windows.Forms.TreeNode[] {
            treeNode13,
            treeNode14,
            treeNode15});
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("入账");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("出账");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("查看账户");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("查看财务记录");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("财务", new System.Windows.Forms.TreeNode[] {
            treeNode17,
            treeNode18,
            treeNode19,
            treeNode20});
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("员工信息");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("公司账户");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("物料组");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("分销渠道");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("交货优先级");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("发运条件");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("管理", new System.Windows.Forms.TreeNode[] {
            treeNode22,
            treeNode23,
            treeNode24,
            treeNode25,
            treeNode26,
            treeNode27});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.注销ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.BackColor = System.Drawing.Color.White;
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.treeView1.Location = new System.Drawing.Point(24, 101);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Node1_1";
            treeNode1.Text = "客户";
            treeNode2.Name = "Node1_2";
            treeNode2.Text = "联系人";
            treeNode3.Name = "Node1_3";
            treeNode3.Text = "销售订单";
            treeNode4.BackColor = System.Drawing.Color.White;
            treeNode4.Name = "Node1";
            treeNode4.Text = "销售";
            treeNode5.Name = "Node2_1";
            treeNode5.Text = "供应商";
            treeNode6.Name = "Node2_2";
            treeNode6.Text = "物料";
            treeNode7.Name = "Node2_3";
            treeNode7.Text = "采购订单";
            treeNode8.Name = "Node2_4";
            treeNode8.Text = "入库";
            treeNode9.Name = "Node2_5";
            treeNode9.Text = "拣料";
            treeNode10.Name = "Node2_6";
            treeNode10.Text = "收货";
            treeNode11.Name = "Node2_7";
            treeNode11.Text = "发货";
            treeNode12.Name = "Node2";
            treeNode12.Text = "物料";
            treeNode13.Name = "Node3_1";
            treeNode13.Text = "车间";
            treeNode14.Name = "Node3_2";
            treeNode14.Text = "生产计划";
            treeNode15.Name = "Node3_3";
            treeNode15.Text = "生产过程";
            treeNode16.Name = "Node3";
            treeNode16.Text = "生产";
            treeNode17.Name = "Node4_1";
            treeNode17.Text = "入账";
            treeNode18.Name = "Node4_2";
            treeNode18.Text = "出账";
            treeNode19.Name = "Node4_3";
            treeNode19.Text = "查看账户";
            treeNode20.Name = "Node4_4";
            treeNode20.Text = "查看财务记录";
            treeNode21.Name = "Node4";
            treeNode21.Text = "财务";
            treeNode22.Name = "Node5_1";
            treeNode22.Text = "员工信息";
            treeNode23.Name = "Node5_2";
            treeNode23.Text = "公司账户";
            treeNode24.Name = "Node5_3";
            treeNode24.Text = "物料组";
            treeNode25.Name = "Node5_4";
            treeNode25.Text = "分销渠道";
            treeNode26.Name = "Node5_5";
            treeNode26.Text = "交货优先级";
            treeNode27.Name = "Node5_6";
            treeNode27.Text = "发运条件";
            treeNode28.Name = "Node5";
            treeNode28.Text = "管理";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode12,
            treeNode16,
            treeNode21,
            treeNode28});
            this.treeView1.Size = new System.Drawing.Size(243, 436);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.注销ToolStripMenuItem,
            this.logoutToolStripMenuItem,
            this.HelpToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 29);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 注销ToolStripMenuItem
            // 
            this.注销ToolStripMenuItem.Name = "注销ToolStripMenuItem";
            this.注销ToolStripMenuItem.Size = new System.Drawing.Size(54, 25);
            this.注销ToolStripMenuItem.Text = "退出";
            this.注销ToolStripMenuItem.Click += new System.EventHandler(this.LogoutToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(54, 25);
            this.logoutToolStripMenuItem.Text = "注销";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.HelpToolStripMenuItem_Click);
            // 
            // HelpToolStripMenuItem1
            // 
            this.HelpToolStripMenuItem1.Name = "HelpToolStripMenuItem1";
            this.HelpToolStripMenuItem1.Size = new System.Drawing.Size(54, 25);
            this.HelpToolStripMenuItem1.Text = "帮助";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(851, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "您好，";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(885, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(984, 579);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 注销ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpToolStripMenuItem1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;

    }
}