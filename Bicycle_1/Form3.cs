﻿using Bicycle;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bicycle_1
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ConfirmPassword_TextChanged(object sender, EventArgs e)
        {
            if (ConfirmPassword.Text != Password.Text)
            {
                errorCP.Visible = true;
                yesButton.Enabled = false;
            }
            else { errorCP.Visible = false; yesButton.Enabled = true; }
        }

        private void Password_TextChanged(object sender, EventArgs e)
        {

        }

        private void id_TextChanged(object sender, EventArgs e)
        {
            //这里判断数据库里是否已经存在输入的工号，若存在，出现红字确认按钮不可用
            //需要一个bool函数IdExist判断数据库的员工表中是否存在(Id.Text)
            StaffServiceClient client = new StaffServiceClient();
            bool result = client.ifexist(Id.Text);
            client.Close();
            if (result)
            {              
                errorID.Visible = true;
                yesButton.Enabled = false;
            }
            else { errorID.Visible = false; yesButton.Enabled = true; }
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void yesButton_Click(object sender, EventArgs e)
        {
            String id = null;
            String name = null;
            String password = null;
            String sex = null;
            String renger = "M";
            String birthday = null;
            String date = null;//进厂时间
            String telephone = null;
            String address = null;
            id = Id.Text;
            name = StaffName.Text;
            password = Password.Text;
            sex = Renger.SelectedItem.ToString();
            if (sex.Equals("男"))
            {
                renger = "M";
            }
            else { renger = "F"; }
            birthday = Birthday.Text;
            date = InDate.Text;
            telephone = Telephone.Text;
            address = Address.Text;
            User.id = id;
            User.name = name;
            User.address = address;
            User.birthday = birthday;
            User.date = date;
            User.password = password;
            User.renger = renger;
            User.telephone = telephone;
            MessageBox.Show("注册成功\n"+User.id + '\n' + User.name + '\n' + User.password + '\n' + User.renger + '\n' + User.birthday + '\n' + User.date + '\n' + User.telephone + '\n' + User.address);
            this.Hide();
            Form4 fm4 = new Form4();
            fm4.ShowDialog();
            StaffServiceClient client = new StaffServiceClient();
            DateTime dt = Convert.ToDateTime(birthday);
            bool a = client.creatstaff(id, name, password, renger, dt, dt, telephone, address);
            client.Close();

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void sex_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void RegisterForm_Load(object sender, EventArgs e)
        {
            this.Renger.SelectedIndex = 0;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
