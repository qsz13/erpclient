﻿namespace Bicycle_1
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterForm));
            this.Id = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Password = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ConfirmPassword = new System.Windows.Forms.TextBox();
            this.yesButton = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.errorCP = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Renger = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Birthday = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.StaffName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Telephone = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Address = new System.Windows.Forms.TextBox();
            this.errorID = new System.Windows.Forms.Label();
            this.InDate = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // Id
            // 
            this.Id.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Id.Location = new System.Drawing.Point(133, 94);
            this.Id.MaxLength = 8;
            this.Id.Name = "Id";
            this.Id.Size = new System.Drawing.Size(120, 26);
            this.Id.TabIndex = 0;
            this.Id.TextChanged += new System.EventHandler(this.id_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(36, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "工号";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(36, 184);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "密码";
            // 
            // Password
            // 
            this.Password.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Password.Location = new System.Drawing.Point(133, 181);
            this.Password.MaxLength = 20;
            this.Password.Name = "Password";
            this.Password.PasswordChar = '●';
            this.Password.Size = new System.Drawing.Size(120, 26);
            this.Password.TabIndex = 3;
            this.Password.TextChanged += new System.EventHandler(this.Password_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(36, 227);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "确认密码";
            // 
            // ConfirmPassword
            // 
            this.ConfirmPassword.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ConfirmPassword.Location = new System.Drawing.Point(133, 224);
            this.ConfirmPassword.MaxLength = 20;
            this.ConfirmPassword.Name = "ConfirmPassword";
            this.ConfirmPassword.PasswordChar = '●';
            this.ConfirmPassword.Size = new System.Drawing.Size(120, 26);
            this.ConfirmPassword.TabIndex = 5;
            this.ConfirmPassword.TextChanged += new System.EventHandler(this.ConfirmPassword_TextChanged);
            // 
            // yesButton
            // 
            this.yesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.yesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.yesButton.Location = new System.Drawing.Point(38, 523);
            this.yesButton.Name = "yesButton";
            this.yesButton.Size = new System.Drawing.Size(167, 35);
            this.yesButton.TabIndex = 6;
            this.yesButton.Text = "确认";
            this.yesButton.UseVisualStyleBackColor = true;
            this.yesButton.Click += new System.EventHandler(this.yesButton_Click);
            // 
            // cancel
            // 
            this.cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cancel.Location = new System.Drawing.Point(222, 523);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(167, 35);
            this.cancel.TabIndex = 7;
            this.cancel.Text = "取消";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // errorCP
            // 
            this.errorCP.AutoSize = true;
            this.errorCP.BackColor = System.Drawing.Color.Transparent;
            this.errorCP.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.errorCP.ForeColor = System.Drawing.Color.Red;
            this.errorCP.Location = new System.Drawing.Point(276, 227);
            this.errorCP.Name = "errorCP";
            this.errorCP.Size = new System.Drawing.Size(116, 17);
            this.errorCP.TabIndex = 8;
            this.errorCP.Text = "两次密码输入不一致";
            this.errorCP.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(36, 271);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "性别";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // Renger
            // 
            this.Renger.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Renger.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Renger.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Renger.FormattingEnabled = true;
            this.Renger.Items.AddRange(new object[] {
            "男",
            "女"});
            this.Renger.Location = new System.Drawing.Point(133, 268);
            this.Renger.Name = "Renger";
            this.Renger.Size = new System.Drawing.Size(120, 28);
            this.Renger.TabIndex = 10;
            this.Renger.SelectedIndexChanged += new System.EventHandler(this.sex_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(36, 314);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "生日";
            // 
            // Birthday
            // 
            this.Birthday.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Birthday.Location = new System.Drawing.Point(133, 308);
            this.Birthday.Name = "Birthday";
            this.Birthday.Size = new System.Drawing.Size(177, 26);
            this.Birthday.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(36, 357);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "进厂时间";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(36, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 20);
            this.label7.TabIndex = 15;
            this.label7.Text = "姓名";
            // 
            // StaffName
            // 
            this.StaffName.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.StaffName.Location = new System.Drawing.Point(133, 138);
            this.StaffName.Name = "StaffName";
            this.StaffName.Size = new System.Drawing.Size(120, 26);
            this.StaffName.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(36, 401);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 20);
            this.label8.TabIndex = 17;
            this.label8.Text = "电话";
            // 
            // Telephone
            // 
            this.Telephone.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Telephone.Location = new System.Drawing.Point(133, 398);
            this.Telephone.MaxLength = 12;
            this.Telephone.Name = "Telephone";
            this.Telephone.Size = new System.Drawing.Size(177, 26);
            this.Telephone.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(36, 444);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "住址";
            // 
            // Address
            // 
            this.Address.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Address.Location = new System.Drawing.Point(133, 441);
            this.Address.MaxLength = 45;
            this.Address.Multiline = true;
            this.Address.Name = "Address";
            this.Address.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Address.Size = new System.Drawing.Size(256, 52);
            this.Address.TabIndex = 20;
            // 
            // errorID
            // 
            this.errorID.AutoSize = true;
            this.errorID.BackColor = System.Drawing.Color.Transparent;
            this.errorID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.errorID.ForeColor = System.Drawing.Color.Red;
            this.errorID.Location = new System.Drawing.Point(276, 97);
            this.errorID.Name = "errorID";
            this.errorID.Size = new System.Drawing.Size(68, 17);
            this.errorID.TabIndex = 22;
            this.errorID.Text = "工号已存在";
            this.errorID.Visible = false;
            // 
            // InDate
            // 
            this.InDate.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.InDate.Location = new System.Drawing.Point(133, 357);
            this.InDate.Name = "InDate";
            this.InDate.Size = new System.Drawing.Size(177, 26);
            this.InDate.TabIndex = 23;
            this.InDate.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(432, 573);
            this.Controls.Add(this.InDate);
            this.Controls.Add(this.errorID);
            this.Controls.Add(this.Address);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Telephone);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.StaffName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Birthday);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Renger);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.errorCP);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.yesButton);
            this.Controls.Add(this.ConfirmPassword);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Id);
            this.Name = "RegisterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "注册";
            this.Load += new System.EventHandler(this.RegisterForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Id;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ConfirmPassword;
        private System.Windows.Forms.Button yesButton;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Label errorCP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Renger;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker Birthday;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox StaffName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Telephone;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Address;
        private System.Windows.Forms.Label errorID;
        private System.Windows.Forms.DateTimePicker InDate;
    }
}