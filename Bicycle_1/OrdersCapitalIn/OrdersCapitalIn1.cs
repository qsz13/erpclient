﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Bicycle_1;

namespace Bicycle
{
    public partial class OrdersCapitalIn1 : Form
    {
        Dictionary<String, decimal> ds = new Dictionary<String, decimal>();
        String id;


        public OrdersCapitalIn1(String orderid)
        {
            InitializeComponent();

            id = orderid;


            MMServiceClient client = new MMServiceClient();
            CustomerServiceClient cclient = new CustomerServiceClient();


            SALES_ORDER so = client.getSalesOrder(id);
            CUSTOMER_INFORMATION c = cclient.getCustomerInfo(so.SO_CUSID.ToString());




            //初始化界面
            string cusid = c.C_ID.ToString();
            string cusname = c.C_NAME;

            string cusaccount = c.C_BANKACCOUNT;
            string payment = so.SO_DOWNPAYMENT.ToString();
            string totalprice = so.SO_TOTALPRICE.ToString();

            label8.Text = cusid;
            label10.Text = cusname;
            label13.Text = orderid;
            label11.Text = cusaccount;
            label12.Text = payment;
            label9.Text = totalprice;




            List<ACCOUNT> alist = new List<ACCOUNT>();
            alist = client.getAccountInformation().ToList();

            for (int i = 0; i < alist.Count; i++)
            {
                ds.Add(alist[i].A_NUMBER.ToString(), Convert.ToDecimal(alist[i].A_BALANCE));
            }
            List<String> accountList = new List<String>();
            foreach (KeyValuePair<String, decimal> cc in ds)
            {
                accountList.Add(cc.Key);
            }
            comboBox1.DataSource = accountList;


           
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void OrdersCapitalIn1_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //打出备注
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            string con = null;





            String inaccount = comboBox1.SelectedItem.ToString();
            String outaccount =  textBox1.Text;
            decimal totalprice = Convert.ToDecimal(label9.Text);

           
            decimal payment = Convert.ToDecimal(label12.Text);


            SDServiceClient sdClient = new SDServiceClient();

            SALES_ORDER so = sdClient.getSalesOrder(id);
            decimal innum = Convert.ToDecimal(textBox3.Text);            
            String discrib = textBox2.Text;
            DateTime time = DateTime.Now;
            MMServiceClient client = new MMServiceClient();
            client.createCapitalIn(time, inaccount, outaccount, textBox3.Text, discrib, "278", id);
            client.updateSOGetMoney(textBox3.Text, id);

            FIServiceClient ficlient = new FIServiceClient();
            ACCOUNT a = ficlient.getAccountInformation(inaccount);
            int accountoldmoney = Convert.ToInt32(a.A_BALANCE);
            decimal nowmoney = innum +Convert.ToDecimal(so.SO_PAIDMONEY);
            decimal accountNewMoney = accountoldmoney + Convert.ToDecimal(textBox3.Text);
            ficlient.updateAcountBalance(inaccount, accountNewMoney.ToString());
            Nullable<decimal> state = sdClient.getSalesOrder(id).SO_SORDERSTATE;
            if(state != null)
            {
                con = state.ToString();
            }
            MessageBox.Show("订单入账成功");
            if (con == null)
            {
                if (nowmoney >= payment) con = "1";
                MessageBox.Show("订单定金支付成功");

            }
            if (con == "3")
            {
                if (nowmoney >= totalprice) con = "4";
                User.paytime = DateTime.Now;
                MessageBox.Show("订单全额支付成功");

            }
            sdClient.updateSalesOrder(id, Convert.ToInt32(con));
            //更新订单
            this.Close();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
