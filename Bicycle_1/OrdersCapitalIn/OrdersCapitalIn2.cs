﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Bicycle_1;

namespace Bicycle
{
    public partial class OrdersCapitalIn2 : Form
    {
        public int sty;
        public OrdersCapitalIn2(string saleorder,int style)
        {
            InitializeComponent();
            textBox1.Text = saleorder;
            sty = style;
        }

        private void OrdersCapitalIn2_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            OrdersCapitalIn3 oi3 = new OrdersCapitalIn3(sty);
            oi3.ShowDialog();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (sty == 1) 
            {
                OrdersCapitalIn1 oi1 = new OrdersCapitalIn1(textBox1.Text);
                oi1.ShowDialog();
                this.Close();
            }
            if (sty == 2)
            {
                SDServiceClient sdClient = new SDServiceClient();

                SALES_ORDER so = sdClient.getSalesOrder(textBox1.Text);
                Nullable<decimal> state = sdClient.getSalesOrder(textBox1.Text).SO_SORDERSTATE;
                string con = null;
                if (state != null)
                {
                    con = state.ToString();
                }
                //判断状态，能进行才进行
                if (con == "1")
                {
                    PickUp1 pu1 = new PickUp1(textBox1.Text);
                    pu1.ShowDialog();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("该订单未付定金!");
                }
            }
            if (sty == 3)
            {
                SDServiceClient sdClient = new SDServiceClient();

                SALES_ORDER so = sdClient.getSalesOrder(textBox1.Text);
                Nullable<decimal> state = sdClient.getSalesOrder(textBox1.Text).SO_SORDERSTATE;
                string con = null;
                if (state != null)
                {
                    con = state.ToString();
                }


                if (con == "2")
                {
                    con = "3";
                    MessageBox.Show("发货成功");
                    User.delivertime = DateTime.Now;
                    sdClient.updateSalesOrder(textBox1.Text, Convert.ToInt32(con));
                    //修改状态
                }
                else
                {
                    MessageBox.Show("该订单未完成拣料");
                }
                this.Close();
            }
            if (sty == 4)
            {
                SalesOrderManagement1 som1 = new SalesOrderManagement1(textBox1.Text);
                som1.ShowDialog();
                this.Close();
            }
            if (sty == 5)
            {
                SalesOrderView1 som1 = new SalesOrderView1(textBox1.Text);
                som1.ShowDialog();
                this.Close();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            MMServiceClient fk = new MMServiceClient();
            CustomerServiceClient customerClient = new CustomerServiceClient();
            if (fk.ifSalesOrderExist(textBox1.Text))
            {
                SALES_ORDER so = new SALES_ORDER();
                so = fk.getSalesOrder(textBox1.Text);
                CUSTOMER_INFORMATION c = new CUSTOMER_INFORMATION();
                c = customerClient.getCustomerInfo(so.SO_CUSID.ToString());
                label2.Text = c.C_NAME;
            }
            else
            {
                label2.Text = "订单ID不存在";
            }
        }


    }
}
