﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class OrdersCapitalIn4 : Form
    {
        public string Company;
        public int sty;
        Dictionary<int, String> resultDic = new Dictionary<int,string>();
        List<string> list = new List<string>();
        public OrdersCapitalIn4(string company,int style)
        {
            InitializeComponent();
            Company = company;
            sty = style;
            MMServiceClient fk = new MMServiceClient();
            List<CUSTOMER_INFORMATION> list = new List<CUSTOMER_INFORMATION>();
            list = fk.searchCustomerID(Company).ToList();
            Dictionary<String, String> dic = new Dictionary<String, String>();

            int i = 0, j = 0;
            foreach (CUSTOMER_INFORMATION c in list)
            {
                dic.Add(c.C_ID.ToString(), c.C_NAME);
                List<SALES_ORDER> Clist = new List<SALES_ORDER>();
                Clist = fk.getSOByCustomer(c.C_ID.ToString()).ToList();
                foreach (SALES_ORDER temp in Clist)
                {
                    //                    MessageBox.Show(temp.PO_ID + "     " + vi.VI_NAME);
                    dataGridView1.RowCount++;

                    dataGridView1.Rows[i].Cells[0].Value = temp.SO_ID.ToString();
                    dataGridView1.Rows[i].Cells[1].Value = c.C_NAME;
                    dataGridView1.Rows[i].Cells[2].Value = temp.SO_ORDERDATE.ToString();
                    dataGridView1.Rows[i].Cells[3].Value = temp.SO_TOTALPRICE.ToString();
                    i = i + 1;

                }
//                lin = dataGridView1.RowCount;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int j = 0; j < dataGridView1.RowCount - 1; j++)
            {
                if (dataGridView1.Rows[j].Cells[4].Value != null) 
                {
                    if (dataGridView1.Rows[j].Cells[4].Value.Equals(true))
                    {
                        OrdersCapitalIn2 oi2 = new OrdersCapitalIn2(dataGridView1.Rows[j].Cells[0].Value.ToString(),sty);
                        oi2.ShowDialog();
                        this.Close();
                        break;
                    }
                }
            }

        }

    }
}
