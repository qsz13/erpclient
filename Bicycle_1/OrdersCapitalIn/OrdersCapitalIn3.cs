﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bicycle
{
    public partial class OrdersCapitalIn3 : Form
    {
        public int sty;
        public OrdersCapitalIn3(int style)
        {
            InitializeComponent();
            sty = style;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OrdersCapitalIn4 oi4 = new OrdersCapitalIn4(textBox1.Text,sty);
            oi4.ShowDialog();
            this.Close();
        }
    }
}
