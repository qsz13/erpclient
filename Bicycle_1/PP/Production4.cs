﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicyclePP
{
    public partial class Production4 : Form
    {
        Dictionary<int, string> Dicworkshop = new Dictionary<int, string>();
        public Production4()
        {
            InitializeComponent();

            Dicworkshop.Add(1, "2");

            List<String> temp = new List<String>();
            foreach (KeyValuePair<int, String> a in Dicworkshop)
            {
                temp.Add(a.Value);
            }
            comboBox1.DataSource = temp;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Production1 pr1 = new Production1(comboBox1.Text,"");
            pr1.ShowDialog();
            this.Hide();
        }
    }
}
