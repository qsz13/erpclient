﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicyclePP
{
    public partial class ProductionPlanManagement2 : Form
    {
        List<WORKSHOP> workshoplist = new List<WORKSHOP>();
        int sty;
        public ProductionPlanManagement2(int style)
        {
            InitializeComponent();
            sty = style;
            PPServiceClient PPClient = new PPServiceClient();
            workshoplist = PPClient.getWorkShop().ToList();

            List<String> temp = new List<String>();
            foreach (WORKSHOP a in workshoplist)
            {
                temp.Add(a.WS_NAME);
            }
            comboBox1.DataSource = temp;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (WORKSHOP a in workshoplist)
            {
                if (a.WS_NAME == comboBox1.Text) 
                {
                    ProductionPlanManagement3 ppm3 = new ProductionPlanManagement3(a.WS_ID.ToString(),sty);
                    ppm3.ShowDialog();
                    this.Hide();
                    break;
                }
            }

        }
    }
}
