﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;


namespace BicyclePP
{
    public partial class ProductionPlanManagement4 : Form
    {
        public string Planid;
        public PRODUCTION_PLAN plan;
        public ProductionPlanManagement4(string planid)
        {
            InitializeComponent();
            Planid = planid;
            PPServiceClient PPClient = new PPServiceClient();
            plan= PPClient.getProductionPlan(planid);
            label8.Text = plan.PP_WORKSHOP.ToString();
            label9.Text=plan.PP_PRODUCTION.ToString();
            label15.Text=plan.PP_PLANNER.ToString();
            label10.Text = plan.PP_CHARGER.ToString();
            label12.Text=plan.PP_STARTTIME.ToString();
            label11.Text=plan.PP_ENDTIME.ToString();
            label13.Text=plan.PP_PLANAMOUNT.ToString();
            label17.Text=plan.PP_PRODUCESTATE.ToString();



        }

        private void ProductionPlanManagement4_Load(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            PPServiceClient PPClient = new PPServiceClient();
            decimal temp1 = 0;
            decimal temp2 = 0;
            temp1 = Convert.ToDecimal(plan.PP_PRODUCEDAMOUNT) + Convert.ToDecimal(textBox2.Text);
            textBox2.Text = "";
            temp2 = Convert.ToDecimal(plan.PP_INAMOUNT) + Convert.ToDecimal(textBox1.Text);
            textBox1.Text = "";
            PPClient.updateProducePlanHaveAmountProduceAmount(Planid, temp1, temp2);
            if (temp1 > plan.PP_PLANAMOUNT)
            {
                PPClient.updateProduceState(Planid, "3");
                MessageBox.Show("储备货物已完成");
            }
            else
            {
                PPClient.updateProduceState(Planid, "2");
                MessageBox.Show("储备货物未完成");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PPServiceClient PPClient = new PPServiceClient();
            decimal temp1 = 0;
            decimal temp2 = 0;
            temp1 = Convert.ToDecimal(plan.PP_PRODUCEDAMOUNT) + Convert.ToDecimal(textBox2.Text);
            textBox2.Text = "";
            temp2 = Convert.ToDecimal(plan.PP_INAMOUNT) + Convert.ToDecimal(textBox1.Text);
            textBox1.Text = "";
            PPClient.updateProducePlanHaveAmountProduceAmount(Planid,temp1,temp2);

            MessageBox.Show("更新成功");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            PPServiceClient PPClient = new PPServiceClient();
            PPClient.updateProduceState(Planid,"4");
            MessageBox.Show("计划取消");
        }
    }
}
