﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;
using Bicycle_1;

namespace BicyclePP
{
    public partial class ProductionPlanCreate3 : Form
    {
        public List<FLOW_LINE_PROCEDURE> List = new List<FLOW_LINE_PROCEDURE>();
        public ProductionPlanCreate3(List<FLOW_LINE_PROCEDURE> list)
        {
            InitializeComponent();
            label8.Text = productionplan.workshopname;
            label10.Text = productionplan.manager;
            label11.Text = productionplan.starttime.ToString();
            label13.Text = productionplan.production;
            label12.Text=productionplan.endtime.ToString();
            label9.Text=productionplan.plannumber;
            List = list;
            //将类的数组读入，显示出来
            for (int j = 0; j < List.Count(); j++) 
            {
                dataGridView1.RowCount = j + 2;
                dataGridView1.Rows[j].Cells[0].Value = List[j].FP_NUM;
                dataGridView1.Rows[j].Cells[1].Value = List[j].FP_DESCRIPTION;
                dataGridView1.Rows[j].Cells[2].Value = List[j].FP_MATERIAL;
                dataGridView1.Rows[j].Cells[3].Value = List[j].FP_AMOUNT;
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //创建生产计划
            PPServiceClient PPClient = new PPServiceClient();
            decimal temp= PPClient.createProductionPlan(productionplan.workshopid,productionplan.starttime, productionplan.endtime,User.id,productionplan.manager,productionplan.production,productionplan.plannumber);
            for(int j=0;j<dataGridView1.RowCount-1;j++)
            {
                PPClient.createFlowLine(dataGridView1.Rows[j].Cells[0].Value.ToString(), temp.ToString(), dataGridView1.Rows[j].Cells[1].Value.ToString(), dataGridView1.Rows[j].Cells[2].Value.ToString(), dataGridView1.Rows[j].Cells[3].Value.ToString());
            } 


            MessageBox.Show("生成成功");
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
