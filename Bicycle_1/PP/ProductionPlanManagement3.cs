﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicyclePP
{
    public partial class ProductionPlanManagement3 : Form
    {
        int sty;
        public ProductionPlanManagement3(string worksid,int style)
        {
            InitializeComponent();
            sty = style;
            PPServiceClient PPClient = new PPServiceClient();
            List<PRODUCTION_PLAN> list = PPClient.getProductionPlanByWorkshop(worksid).ToList<PRODUCTION_PLAN>();
            for (int j = 0; j < list.Count; j++)
            {
                dataGridView1.RowCount = j + 2;
                dataGridView1.Rows[j].Cells[0].Value = list[j].PP_ID;
                dataGridView1.Rows[j].Cells[1].Value = list[j].PP_STARTTIME.ToString();
                dataGridView1.Rows[j].Cells[2].Value = list[j].PP_ENDTIME.ToString();
                dataGridView1.Rows[j].Cells[3].Value = list[j].PP_CHARGER;
                dataGridView1.Rows[j].Cells[4].Value = list[j].PP_PRODUCESTATE;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int j = 0; j < dataGridView1.RowCount - 1; j++)
            {
                if (dataGridView1.Rows[j].Cells[5].Value != null)
                {
                    if (dataGridView1.Rows[j].Cells[5].Value.Equals(true))
                    {
                        ProductionPlanManagement1 ppm1 = new ProductionPlanManagement1(dataGridView1.Rows[j].Cells[0].Value.ToString(),sty);
                        ppm1.ShowDialog();
                        this.Hide();
                    }

                }
            }
        }
    }
}
