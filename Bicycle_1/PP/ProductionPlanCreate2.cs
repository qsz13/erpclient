﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicyclePP
{
    public partial class ProductionPlanCreate2 : Form
    {
        public ProductionPlanCreate2()
        {
            InitializeComponent();
        }

        private void ProductionPlanCreate2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<FLOW_LINE_PROCEDURE> list = new List<FLOW_LINE_PROCEDURE>();
            
            for (int j = 0; j < dataGridView1.RowCount-1; j++)
            {
                if (dataGridView1.Rows[j].Cells[0].Value != null)
                {
                    FLOW_LINE_PROCEDURE flow = new FLOW_LINE_PROCEDURE();
                    flow.FP_NUM =Convert.ToDecimal( dataGridView1.Rows[j].Cells[0].Value);
                    flow.FP_DESCRIPTION = dataGridView1.Rows[j].Cells[1].Value.ToString();
                    flow.FP_MATERIAL = Convert.ToDecimal(dataGridView1.Rows[j].Cells[2].Value);
                    flow.FP_AMOUNT = Convert.ToDecimal(dataGridView1.Rows[j].Cells[3].Value);
                    list.Add(flow);
                }
                //将信息记录到类的数组中
            }
            ProductionPlanCreate3 ppc3 = new ProductionPlanCreate3(list);
            ppc3.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
