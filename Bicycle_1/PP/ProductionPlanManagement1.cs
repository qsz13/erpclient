﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicyclePP
{
    public partial class ProductionPlanManagement1 : Form
    {
        public int sty;
        public ProductionPlanManagement1(string planid,int style)
        {
            InitializeComponent();
            textBox1.Text = planid;
            sty = style;
        }

        private void ProductionPlanManagement1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProductionPlanManagement2 ppm2 = new ProductionPlanManagement2(sty);
            ppm2.ShowDialog();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (sty == 1)
            {
                ProductionPlanManagement4 ppm4 = new ProductionPlanManagement4(textBox1.Text);
                ppm4.ShowDialog();
                this.Hide();
            }
            if (sty == 2)
            {
                ProductionPlanView4 ppm4 = new ProductionPlanView4(textBox1.Text);
                ppm4.ShowDialog();
                this.Hide();
            }
            
        }
    }
}
