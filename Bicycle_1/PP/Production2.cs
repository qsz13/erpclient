﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicyclePP
{
    public partial class Production2 : Form
    {
        public string WorkShop;
        public Production2(string workshop)
        {
            InitializeComponent();
            WorkShop = workshop;

            int j = 1;
            for (int i = 0; i < 2; i++)//读取所有计划
            {
                j++;
                dataGridView1.RowCount = j;
                dataGridView1.Rows[j - 2].Cells[0].Value = j.ToString();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int j = 0; j < dataGridView1.RowCount - 1; j++)
            {
                if (dataGridView1.Rows[j].Cells[5].Value != null) 
                {
                    if (dataGridView1.Rows[j].Cells[5].Value.Equals(true))
                    {
                        Production1 pr1 = new Production1(WorkShop, dataGridView1.Rows[j].Cells[0].Value.ToString());
                        pr1.ShowDialog();
                        this.Hide();
                        break;
                    }
                }
            }

        }
    }
}
