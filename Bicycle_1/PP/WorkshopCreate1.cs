﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicyclePP
{
    public partial class WorkshopCreate1 : Form
    {
        Dictionary<int, String> Dicmaterialgroup = new Dictionary<int, String>();
        public WorkshopCreate1()
        {
            InitializeComponent();
            //读取物料组别
            MMServiceClient MMClient = new MMServiceClient();
            Dicmaterialgroup=MMClient.getAllMaterialGroup();

            List<String> temp = new List<String>();
            foreach (KeyValuePair<int, String> a in Dicmaterialgroup)
            {
                temp.Add(a.Value);
            }
            comboBox1.DataSource = temp;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //将各种label值添加为一个车间
            PPServiceClient PPClient = new PPServiceClient();
            string materialid="";
            foreach (KeyValuePair<int, String> a in Dicmaterialgroup)
            {
                if (a.Value == comboBox1.Text)
                {
                    materialid = a.Key.ToString();
                    break;
                }
            }               
            PPClient.createWorkShop(textBox1.Text,materialid,textBox3.Text,textBox4.Text);
            MessageBox.Show("车间生成完毕");
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
