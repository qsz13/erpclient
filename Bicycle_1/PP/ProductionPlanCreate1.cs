﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicyclePP
{
    public partial class ProductionPlanCreate1 : Form
    {
        List<WORKSHOP> workshoplist = new List<WORKSHOP>();
        public ProductionPlanCreate1()
        {
            InitializeComponent();
            PPServiceClient PPClient = new PPServiceClient();
            workshoplist = PPClient.getWorkShop().ToList();
            
            List<String> temp = new List<String>();
            foreach (WORKSHOP a in workshoplist)
            {
                temp.Add(a.WS_NAME);
            }
            comboBox1.DataSource = temp;
        }

        private void ProductionPlanCreate1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            productionplan.workshopname = comboBox1.Text;
            foreach (WORKSHOP a in workshoplist)
            {
                productionplan.workshopid = a.WS_ID.ToString();
                break;
            }
            productionplan.starttime = dateTimePicker1.Value;
            productionplan.endtime = dateTimePicker2.Value;
            productionplan.manager = textBox4.Text;
            productionplan.production = textBox5.Text;
            productionplan.plannumber = textBox6.Text;
            ProductionPlanCreate2 ppc2 = new ProductionPlanCreate2();

            this.Hide();
            ppc2.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
