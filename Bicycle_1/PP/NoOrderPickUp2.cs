﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicyclePP
{
    public partial class NoOrderPickUp2 : Form
    {
        public NoOrderPickUp2()
        {
            InitializeComponent();

            MMServiceClient mmsclient = new MMServiceClient();
            List<MATERIAL> list = mmsclient.getAllMaterial().ToList<MATERIAL>();

            for (int i = 0; i < list.Count; i++)//读取所有物料
            {
                dataGridView1.RowCount = i+2;
                dataGridView1.Rows[i].Cells[0].Value = list[i].M_MATERIALID;
                dataGridView1.Rows[i].Cells[1].Value = list[i].M_NAME;
                dataGridView1.Rows[i].Cells[2].Value = list[i].M_DESCRIPTION;
            }

            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            for (int j = 0; j < dataGridView1.RowCount - 1; j++)
            {
                if (dataGridView1.Rows[j].Cells[3].Value != null) 
                {
                    if (dataGridView1.Rows[j].Cells[3].Value.Equals(true))
                    {
                        NoOrderPickUp1 oi2 = new NoOrderPickUp1(dataGridView1.Rows[j].Cells[0].Value.ToString());
                        oi2.ShowDialog();
                        this.Close();
                        break;
                    }
                }
            }
        }
    }
}
