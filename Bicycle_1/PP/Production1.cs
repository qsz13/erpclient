﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicyclePP
{
    public partial class Production1 : Form
    {
        public string WorkShop;
        public Production1(string workshop,string orderid)
        {
            InitializeComponent();
            WorkShop = workshop;
            textBox1.Text = orderid;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Production3 pr3 = new Production3(WorkShop,textBox1.Text);
            pr3.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Production2 pr2 = new Production2(WorkShop);
            pr2.ShowDialog();
            this.Close();
        }
    }
}
