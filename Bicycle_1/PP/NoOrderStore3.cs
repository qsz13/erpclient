﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicyclePP
{
    public partial class NoOrderStore3 : Form
    {
        MATERIAL ma=new MATERIAL();
        public NoOrderStore3(string materialid)
        {
            InitializeComponent();

            MMServiceClient mmsclient = new MMServiceClient();
            ma = mmsclient.getMaterial(materialid);

            String name = ma.M_NAME;
            String group = ma.M_MGID.ToString();
            String description = ma.M_DESCRIPTION;
            String remainnumber = ma.M_LEFTAMOUT.ToString();

            label7.Text = materialid;
            label10.Text = name;
            label9.Text = group;
            label12.Text = description;
            label8.Text = remainnumber;
        }

        private void NoOrderStore3_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SDServiceClient sds = new SDServiceClient();
            sds.updateMaterial(ma.M_MATERIALID.ToString(),textBox1.Text);
            MessageBox.Show("入库成功");
            //提交
            label8.Text = (Convert.ToInt32(label8.Text) - Convert.ToInt32(textBox1.Text)).ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
