﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicyclePP
{
    public partial class ProductionPlanView4 : Form
    {
        public ProductionPlanView4(string planid)
        {
            InitializeComponent();

            PPServiceClient PPClient = new PPServiceClient();
            PRODUCTION_PLAN plan = PPClient.getProductionPlan(planid);
            label12.Text = plan.PP_WORKSHOP.ToString();
            label17.Text = plan.PP_PRODUCTION.ToString();
            label20.Text = plan.PP_PLANNER.ToString();
            label14.Text = plan.PP_CHARGER.ToString();
            label18.Text = plan.PP_STARTTIME.ToString();
            label13.Text = plan.PP_ENDTIME.ToString();
            label16.Text = plan.PP_PLANAMOUNT.ToString();
            label22.Text = plan.PP_PRODUCESTATE.ToString();
            label19.Text=  plan.PP_PRODUCEDAMOUNT.ToString();
            label21.Text=  plan.PP_INAMOUNT.ToString();
            label15.Text=  plan.PP_PRODUCESTATE.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProductionPlanManagement1 ppm1 = new ProductionPlanManagement1("", 2);
            ppm1.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
