﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BicyclePP
{
    public partial class NoOrderPickUp1 : Form
    {
        public NoOrderPickUp1(string materialid)
        {
            InitializeComponent();
            textBox1.Text = materialid;
        }

        private void NoOrderPickUp1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            NoOrderPickUp2 nopu2 = new NoOrderPickUp2();
            nopu2.ShowDialog();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NoOrderPickUp3 nopu3 = new NoOrderPickUp3(textBox1.Text);
            nopu3.ShowDialog();
            this.Hide();
        }
    }
}
