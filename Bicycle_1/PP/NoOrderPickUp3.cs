﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ERPServer;

namespace BicyclePP
{
    public partial class NoOrderPickUp3 : Form
    {
        MATERIAL ma=new MATERIAL();
        public NoOrderPickUp3(string materialid)
        {
            InitializeComponent();

            MMServiceClient mmsclient = new MMServiceClient();
            ma= mmsclient.getMaterial(materialid);

            String name = ma.M_NAME;
            String group = ma.M_MGID.ToString();
            String description = ma.M_DESCRIPTION;
            String remainnumber = ma.M_LEFTAMOUT.ToString();

            label7.Text = materialid;
            label10.Text = name;
            label9.Text = group;
            label12.Text = description;
            label8.Text = remainnumber;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SDServiceClient sds = new SDServiceClient();
            sds.updateMaterial(ma.M_MATERIALID.ToString(),((-1)*Convert.ToInt32(textBox1.Text)).ToString());
            //提交
            MessageBox.Show("拣料成功");
            label8.Text = (Convert.ToInt32(label8.Text) - Convert.ToInt32(textBox1.Text)).ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
