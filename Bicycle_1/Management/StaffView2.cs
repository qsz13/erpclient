﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Management
{
    public partial class StaffView2 : Form
    {
        String id;
        public StaffView2(String a)
        {
            InitializeComponent();
            StaffServiceClient sm = new StaffServiceClient();
            id = a;
            STAFF_INFORMATION si = sm.info(id);
            label23.Text = id;
            textBox3.Text = si.S_NAME;
            textBox4.Text = si.S_PASSWORD;
            List<String> list = new List<String>();
            list.Add("F");
            list.Add("M");
            comboBox1.DataSource = list;
            comboBox1.SelectedItem = si.S_GENDER;
            dateTimePicker1.Value = si.S_BIRTHDAY;
            dateTimePicker2.Value = si.S_ICDATE;
            textBox5.Text = si.S_TEL;
            textBox6.Text = si.S_ADDRESS;
            List<decimal> sdlist = new List<decimal>();
            sdlist.Add(0);
            sdlist.Add(1);
            List<decimal> mmlist = new List<decimal>();
            mmlist.Add(0);
            mmlist.Add(1);
            List<decimal> pplist = new List<decimal>();
            pplist.Add(0);
            pplist.Add(1);
            List<decimal> filist = new List<decimal>();
            filist.Add(0);
            filist.Add(1);
            List<decimal> malist = new List<decimal>();
            malist.Add(0);
            malist.Add(1);
            comboBox2.DataSource = sdlist;
            comboBox2.SelectedItem = si.S_SDABLE;
            comboBox3.DataSource = mmlist;
            comboBox3.SelectedItem = si.S_MMABLE;
            comboBox4.DataSource = pplist;
            comboBox4.SelectedItem = si.S_PPEABLE;
            comboBox5.DataSource = filist;
            comboBox5.SelectedItem = si.S_FIABLE;
            comboBox6.DataSource = malist;
            comboBox6.SelectedItem = si.S_MAABLE;
        }

        private void StaffView2_Load(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StaffServiceClient sf = new StaffServiceClient();
            STAFF_INFORMATION si = new STAFF_INFORMATION();
            si.S_ID = Convert.ToDecimal(id);
            si.S_NAME = textBox3.Text;
            si.S_PASSWORD = textBox4.Text;
            si.S_GENDER = comboBox1.SelectedItem.ToString();
            si.S_BIRTHDAY = dateTimePicker1.Value;
            si.S_ICDATE = dateTimePicker2.Value;
            si.S_TEL = textBox5.Text;
            si.S_ADDRESS = textBox6.Text;
            si.S_SDABLE = comboBox2.SelectedIndex;
            si.S_MMABLE = comboBox3.SelectedIndex;
            si.S_PPEABLE = comboBox4.SelectedIndex;
            si.S_FIABLE = comboBox5.SelectedIndex;
            si.S_MAABLE = comboBox6.SelectedIndex;
            sf.updateStaff(si);
            MessageBox.Show(textBox3.Text + "修改成功");
            this.Close();


        }

        private void button4_Click(object sender, EventArgs e)
        {
            StaffServiceClient sf = new StaffServiceClient();
            sf.deleteStaff(id);
            MessageBox.Show(textBox3.Text + "删除成功");
            this.Close();
        }
    }
}
