﻿using ERPServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Management
{
    public partial class StaffManagement : Form
    {
        int lin = 0;
        public StaffManagement()
        {
            InitializeComponent();
            StaffServiceClient sm = new StaffServiceClient();
             List<STAFF_INFORMATION> slist=new List<STAFF_INFORMATION>();
            slist=sm.getAllStaffInfo().ToList();
            int i=0;
            foreach(STAFF_INFORMATION si in slist)
            {
                dataGridView1.RowCount++;
                dataGridView1.Rows[i].Cells[0].Value = si.S_ID.ToString();
                dataGridView1.Rows[i].Cells[1].Value = si.S_NAME;
                dataGridView1.Rows[i].Cells[2].Value = si.S_GENDER;
                dataGridView1.Rows[i].Cells[3].Value = si.S_ICDATE.ToString();
                i = i + 1;
            }
            lin = i;
        }

        private void StaffManagement_Load(object sender, EventArgs e)
        {

            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int result = 0;
            for (int j = 0; j < lin; j++)
            {
                if (dataGridView1.Rows[j].Cells[4].Value != null)
                {
                    result = j;
                }
            }
            StaffView2 sv2 = new StaffView2(dataGridView1.Rows[result].Cells[0].Value.ToString());
            sv2.ShowDialog();
            this.Hide();
        }
    }
}
